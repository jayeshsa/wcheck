app.controller('SearchCtrl', function($scope,$timeout,wecheckApi,$rootScope,Loading,errorPopup,$state,$cordovaGeolocation ) {
$scope.search_data=[];
	$rootScope.search=function()
	{	
		console.log($scope.search_text)
		if($scope.search_text==null || $scope.search_text.length<1)
		{
			errorPopup.showPopup("Alert!!","Please enter search text","error");
			return;
		}
		
		Loading.show();
		wecheckApi.getsearch(""+$scope.search_text).then(function(res){
			
			Loading.hide();
			console.log(res);
			$scope.search_data=res.data;
			
 $scope.get_location();
		},function(err){
			console.log(err)
		})    
	}




	  $scope.viewVenuInfo = function(venue) {
        wecheckApi.setShareData('venue', venue);
        $state.go('app.venuInfo');
    };

    var currentPos = {};

    var calculateDistance = function () {
        for (var i = 0; i < $scope.search_data.length; i++) {
            $scope.search_data[i].distance =
                wecheckApi.calcDistance(currentPos.latitude, currentPos.longitude, $scope.search_data[i].latitude, $scope.search_data[i].longitude);
        }
    }

	

    $scope.get_location=function()
    {

        $cordovaGeolocation.getCurrentPosition({
                timeout: 10000,
                enableHighAccuracy: false
            }).then(function (position) {
                currentPos = position.coords;
                wecheckApi.setShareData('currentPos', {
                    latitude: currentPos.latitude,
                    longitude: currentPos.longitude
                });

                if ($scope.search_data.length > 0) calculateDistance();

                // get current address
                var geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(currentPos.latitude, currentPos.longitude);

                geocoder.geocode({ location: latlng }, function(data, status) {
                    if (status == google.maps.GeocoderStatus.OK && data[0]) {
                            wecheckApi.getUser().address = data[0].formatted_address;
                    }
                });
            }, function (err) {
                console.log(err);
            });
    }

// $rootScope.search();


	
})