'use strict'
angular.module('starter.controllers')
    .controller('VenuSocialCtrl', function($scope,$ionicPlatform, $sce, $ionicLoading, vgFullscreen, $http, errorPopup, wecheckApi,TwitterREST) {
    
    var venue = wecheckApi.getShareData('venue');
    $scope.venue = venue;
    console.log(venue);

     $scope.$on("$ionicView.loaded", function(event, data){
             $ionicPlatform.on('resume', function(){
                $scope.loading_socialdata=true;
                $scope.doRefresh_Social(true);
            });
            $scope.doRefresh_Social(true);
    });

    $scope.doRefresh_Social=function(flag)
    {
        $scope.check_wifi();
        $scope.Social_data = []; 
        $scope.load_data(flag);  
    }

     $scope.load_data=function(loader){
        if(loader){$ionicLoading.show({templateUrl: 'templates/spinner.html'});}
        wecheckApi.getInstagram(venue.id).success(function (res) {
            // console.log(res)
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
            $scope.instagram = res;
            $scope.instagram_data = res.media_data;
            
        });
   }

//      $scope.load_data=function(loader){
//         if(loader){$ionicLoading.show({templateUrl: 'templates/spinner.html'});}
//         wecheckApi.getInstagram(venue.id).success(function (res) {
//             console.log(res)
//             $ionicLoading.hide();
//             $scope.$broadcast('scroll.refreshComplete');
//             $scope.instagram = res;
//             if (res.media && res.media.length > 2) {
//                 $scope.instagram.media = res.media.slice(0, 2);
//             }
//         });
//    }

// + venue.facebook_link
    //   if (venue.facebook_link) {
    //      $http.get('https://graph.facebook.com/v2.8/?access_token=541176595984451|K6QoK36SKHoX4cL4xrMM5pLAq5c&ids'+venue.facebook_link ).success(function (res) {
            
    //          console.log(res);
    //          var url = "https://graph.facebook.com/v2.4/" + res.venue.facebook_link.id + "/feed?fields=id,from,name,message,created_time,story,description,link&limit=10&access_token=541176595984451|K6QoK36SKHoX4cL4xrMM5pLAq5c&callback=";
    //          $http.get(url).success(function (res) {
    //            $scope.facebook_data=res;
    //            console.log(res);  
    //          });
    //      })
    //     }

 
        // $http.get("https://graph.facebook.com/v2.4/1195626320544250/feed?fields=id,from,name,message,created_time,icon,story,picture,description,link&limit=4&access_token=541176595984451|K6QoK36SKHoX4cL4xrMM5pLAq5c&callback=").then(function(res){
        //     console.log(res.data);
        //   });

       

        // get facebook id from url
        if (venue.facebook_link) {
            console.log(venue.facebook_link);
         $http.get('https://graph.facebook.com/v2.8/?access_token=541176595984451|K6QoK36SKHoX4cL4xrMM5pLAq5c&ids=' + venue.facebook_link).success(function (res) {
             if (Object.keys(res).length == 0) return;         
             var result = res[Object.keys(res)[0]];
             console.log(result);

             $http.get("https://graph.facebook.com/v2.4/" + result.id + "/feed?fields=id,from,name,message,created_time,icon,story,picture,description,link&limit=10&access_token=541176595984451|K6QoK36SKHoX4cL4xrMM5pLAq5c&callback=").then(function(res){
            console.log(res.data);
            $scope.facebook_data = res.data;
          });


            //  var url = "https://graph.facebook.com/v2.4/" + result.id + "/feed?fields=id,from,name,message,created_time,story,description,link&limit=3&access_token=541176595984451|K6QoK36SKHoX4cL4xrMM5pLAq5c&callback=";
            //  jQuery('.social-feed-container').socialfeed({
            //      // FACEBOOK
            //      facebook: {
            //          accounts: ['!' + result.id],
            //          limit: 5,
            //          access_token: '541176595984451|K6QoK36SKHoX4cL4xrMM5pLAq5c'
            //      }
            //  });
         })
        }

        // if (venue.twitter_link) {
        //     // extract twitter name
        //     console.log(venue.twitter_link);
        //     var query = venue.twitter_link.substr(venue.twitter_link.lastIndexOf("/"));
        //     query = query.replace("/", "");
        //     console.log(query);
        //     jQuery('.social-feed-container').socialfeed({
        //         // Twitter
        //         twitter: {
        //             accounts: ['#' + query],
        //             limit: 3,
        //             consumer_key: 'qzRXgkI7enflNJH1lWFvujT2P', // make sure to have your app read-only
        //             consumer_secret: '8e7E7gHuTwyDHw9lGQFO73FcUwz9YozT37lEvZulMq8FXaPl8O', // make sure to have your app read-only
        //         }
        //     });
        // }

        // if (venue.instagram_link) {
        //     // extract twitter name
        //     var query = venue.instagram_link.substr(venue.instagram_link.lastIndexOf("/"));
        //     query = query.replace("/", "");
        //     console.log(query);
        //     jQuery('.social-feed-container').socialfeed({
        //         instagram: {
        //             accounts: ['@' + query],
        //             limit: 3,
        //             access_token: '5331120822.90248c8.bc4947afa7104f6aa1dab2c16552153d'
        //         },
        //     });
        // }

        TwitterREST.sync().then(function(tweets){
        console.log(tweets);
        $scope.tweets = tweets.statuses;
    });

        $scope.innapBrowser = function (value) {
            window.open(value, '_blank');
        };


        $scope.open_web=function(link)
        {
            window.open(link, '_system', 'location=yes');
        }
    })