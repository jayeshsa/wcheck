"use strict"
angular.module('starter.controllers')
.controller('EventListCtrl', function($scope,$ionicPlatform, $state, $cordovaGeolocation, $ionicLoading, Setting, $rootScope, $cordovaDialogs, $stateParams, wecheckApi) {
$ionicPlatform.on('resume', function(){
      $scope.check_wifi();
});
  
   
    $scope.data = {};

    $scope.data.sliderOptions = {
        loop: true,
        autoplay: 2000,
        initialSlide: 0,
        direction: 'horizontal', //or vertical
        speed: 500 //0.3s transition
    };

    if (window.cordova) {
        cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
            if (enabled == false) {
                $cordovaDialogs.alert('Wecheck wants to access your location while you use the app?', 'Allow', 'OK').then(function() {
                    cordova.plugins.diagnostic.switchToLocationSettings();
                    // $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true});
                });
            }
        }, function(error) {
            console.error(error);
        });
    }

    $scope.data.sliderDelegate = null;

    //watch our sliderDelegate reference, and use it when it becomes available
    $scope.$watch('data.sliderDelegate', function(newVal, oldVal) {
        if (newVal != null) {
            $scope.data.sliderDelegate.on('slideChangeEnd', function() {
                $scope.data.currentPage = $scope.data.sliderDelegate.activeIndex;
                //use $scope.$apply() to refresh any content external to the slider
                $scope.$apply();
            });
        }
    });

    $scope.homepageSliders = wecheckApi.getShareData('homepageSliders', []);

    $scope.favoriteEvent = function($event, event) {
        wecheckApi.favoriteEvent(event.id, !event.favorited).success(function (res) {
            event.favorited = !event.favorited;
        });

        $event.stopPropagation();
        $event.preventDefault();
    };

    $scope.viewEventInfo = function(event) {

        wecheckApi.setShareData('event', event);
        $state.go('app.eventInfo');
    };

    var posOptions = {
        timeout: 10000,
        enableHighAccuracy: false
    };

    var currentPos = wecheckApi.getShareData('currentPos', {});

    var calculateDistance = function () {
        for (var i = 0; i < $scope.events.length; i++) {
            $scope.events[i].distance =
                wecheckApi.calcDistance(currentPos.latitude, currentPos.longitude, $scope.events[i].latitude, $scope.events[i].longitude);
        }
    }

    // scratch data from the server
    $scope.events = [];
    wecheckApi.getEvents().success(function(res) {
        $scope.events = res;
        if (currentPos.latitude) calculateDistance();
    }).error(function () {
        console.log('error');
    });
})