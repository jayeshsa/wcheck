app.controller('AccommodationCtrl', function($scope, $state, $cordovaGeolocation, $ionicLoading, Setting, $rootScope, $cordovaDialogs,
 $filter, wecheckApi, wecheckStorage,$ionicPlatform,errorPopup,$http, $ionicModal, $ionicPopup) {

    var mainUrl = "http://wecheck.co.za/cms/wecheck_oms/";
    var homedata_loadUrl = "homedata_load.php";
  // window.localStorage.setItem('guestLists', '');

   $scope.loading_homedata=true;

  // $scope.guestLists = [{
  //   id: 1,
  //   name: "List1",
  //   side: "Bride",
  //   RSVP: "Yes",
  //   attending: "Not Decided"
  // }];



  $scope.guestLists = [];

   var guestStringDataSaved = window.localStorage.getItem('guestLists');
   if(guestStringDataSaved){
     var itemStrList = guestStringDataSaved.split('----------');
     if(itemStrList.length > 0){
       for(var i = 0; i < itemStrList.length; i++){
         $scope.guestLists.push(JSON.parse(itemStrList[i]));
       }
     }
   }




    // Opens ionicModal when we add a guest
    $ionicModal.fromTemplateUrl('partials/addGuestList.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.guestList = {
        name: "",
        side: "Bride",
        RSVP: "No",
        attending: "Not Decided"
    };

    // Add new Guest List
    $scope.saveGuestList = function() {
      if(!$scope.guestList.name){
        var alertPopup = $ionicPopup.alert({
          title: 'Warning',
          template: 'Enter Guest Name!!!'
        });
        return;
      }



        $scope.guestLists.push($scope.guestList);

        //save guestLists on localstorage
        var guestStringDataToSaveOnLocalstorage = '';
        if($scope.guestLists.length == 0){
          guestStringDataToSaveOnLocalstorage = JSON.stringify($scope.guestList);
        }
        else if($scope.guestLists.length > 0){
          for(var i = 0; i < $scope.guestLists.length; i++){
            if(i == 0){
              guestStringDataToSaveOnLocalstorage = JSON.stringify($scope.guestLists[i]);
              continue;
            }
            guestStringDataToSaveOnLocalstorage += "----------" + JSON.stringify($scope.guestLists[i]);
          }
        }
        window.localStorage.setItem('guestLists', guestStringDataToSaveOnLocalstorage);
        //////////////////////////////////////////////////////////////////////////////

        $scope.guestList = {
            name: "",
            side: "Bride",
            RSVP: "No",
            attending: "Not Decided"
        };
        $scope.modal.hide();
    };



   $scope.Home_data = [];
$scope.for_nodata_json = [];
   $scope.filter_for_accommodation = function(str){
        n = str.includes("Accommodation");
     if(n==true){
            $scope.for_nodata_json.push({"a":"b"});
     }
        return n;
   };
console.log($scope.for_nodata_json);

   $scope.load_data=function(loader){

       if(loader){$ionicLoading.show({templateUrl: 'templates/spinner.html'});}

        wecheckApi.getVenues().success(function(res) {
            console.log("::Accommodation:: " + res);
             $scope.refresh_inbox();
            $scope.$broadcast('scroll.refreshComplete');
            $scope.loading_homedata=false;


            $scope.homepageSliders = res.homepage_sliders;
            $scope.Home_data = res.venues;
            if (currentPos.latitude) calculateDistance();
            wecheckApi.setShareData('homepageSliders', $scope.homepageSliders);
             $scope.get_location();

        }).error(function () {
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
            // $scope.loading_homedata=false;

        });
   };

    $scope.$on("$ionicView.loaded", function(event, data){
             $ionicPlatform.on('resume', function(){
                $scope.loading_homedata=true;
                $scope.doRefresh_Home(true);
            });
            $scope.doRefresh_Home(true);
    });

    $scope.doRefresh_Home=function(flag)
    {
        $scope.check_wifi();
        $scope.Home_data = [];
        $scope.load_data(flag);
    };






    $scope.data = {};
    $scope.data.sliderOptions = {
        loop: true,
        autoplay: 2000,
        initialSlide: 0,
        direction: 'horizontal', //or vertical
        speed: 500 //0.3s transition
    };


    $scope.data.sliderDelegate = null;


    $scope.$watch('data.sliderDelegate', function(newVal, oldVal) {
        if (newVal != null) {
            $scope.data.sliderDelegate.on('slideChangeEnd', function() {
                $scope.data.currentPage = $scope.data.sliderDelegate.activeIndex;
                $scope.$apply();
            });
        }
    });




    $scope.favoriteVenue = function($event, venue) {
        wecheckApi.favoriteVenue(venue.id, !venue.favorited).success(function (res) {venue.favorited = !venue.favorited;});
        $event.stopPropagation();
        $event.preventDefault();
    };

    $scope.viewVenuInfo = function(venue) {
        wecheckApi.setShareData('venue', venue);
        $state.go('app.venuInfo');
    };

    var currentPos = {};

    var calculateDistance = function () {
        for (var i = 0; i < $scope.Home_data.length; i++) {
            $scope.Home_data[i].distance =
                wecheckApi.calcDistance(currentPos.latitude, currentPos.longitude, $scope.Home_data[i].latitude, $scope.Home_data[i].longitude);
        }
    };

    $scope.get_location=function()
    {

        $cordovaGeolocation.getCurrentPosition({
                timeout: 10000,
                enableHighAccuracy: false
            }).then(function (position) {
                currentPos = position.coords;
                wecheckApi.setShareData('currentPos', {
                    latitude: currentPos.latitude,
                    longitude: currentPos.longitude
                });

                if ($scope.Home_data.length > 0) calculateDistance();

                // get current address
                var geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(currentPos.latitude, currentPos.longitude);

                geocoder.geocode({ location: latlng }, function(data, status) {
                    if (status == google.maps.GeocoderStatus.OK && data[0]) {
                            wecheckApi.getUser().address = data[0].formatted_address;
                    }
                });
            }, function (err) {
                console.log(err);
            });
    };





    // get new message count
    wecheckStorage.getMessages().then(function (res) {
        var user = wecheckApi.getUser();
        $rootScope.newMailCount = $filter('filter')(res, {is_new: 1}).length;
        $rootScope.newMailCount += user.mail_count * 1;

    });

    // var geocoder = new google.maps.Geocoder();
    // var latlng = new google.maps.LatLng(52.361427, 4.8552631);
    //
    // geocoder.geocode({ location: latlng }, function(data, status) {
    //     console.log(data);
    // });

    // =============================== UMesh ===================================
    // $http.post(mainUrl+homedata_loadUrl).then(function(res){
    //         console.log(res.data);
    //       });



});

