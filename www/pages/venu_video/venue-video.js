angular.module('starter.controllers')
.controller('VenuVideoCtrl', function($scope,$ionicPlatform,$ionicModal, $sce,$timeout, vgFullscreen, wecheckApi,$ionicLoading) {
 $ionicPlatform.on('resume', function(){
      $scope.check_wifi();
});


    var playerAPI = {};
    var venue;
    $ionicLoading.show({templateUrl: 'templates/spinner.html'});

    setTimeout(function(){ 
        $ionicLoading.hide();
     }, 2000);
     
    venue = wecheckApi.getShareData('venue');
    console.log(venue);
   
    $scope.venue = venue;
    $scope.video_url = $sce.trustAsResourceUrl(venue.video_url);
   
 

    $scope.config = {
        sources: [
            {src: venue.video_url},
            {src: $sce.trustAsResourceUrl(venue.video_url), type: "video/mp4"},
        ],
        tracks: [
            {
                src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                kind: "subtitles",
                srclang: "en",
                label: "English",
                default: ""
            }
        ],
        theme: "lib/videogular-themes-default/videogular.css",
        plugins: {
            poster: ""
            // poster: "http://www.videogular.com/assets/images/videogular.png"
        }
    };

      $scope.playVideo = function() {
        $scope.showModal('templates/video-popover.html');
    }

  

    $scope.onPlayerReady = function(API) {
        playerAPI = API;
    };

    document.addEventListener(vgFullscreen.onchange, function() {
        document.addEventListener("deviceready", function() {
            if (playerAPI.isFullScreen) {
                screen.lockOrientation('portrait');
            } else {
                screen.lockOrientation('landscape');
            }
        }, false);
    }, false);

    

      $scope.showModal = function(templateUrl) {
        $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    }
    
    $scope.closeModal = function() {
        $scope.modal.hide();
        // $scope.modal.remove();
        $scope.modal.remove();
    };

        $ionicPlatform.onHardwareBackButton(function() {
            $scope.modal.hide();
            $scope.modal.remove();
            console.log("ionic back button push")
        });

})