angular.module('starter.controllers').controller('VenuGalleryCtrl', function($scope,$ionicPlatform, CameraServ, wecheckApi) {
 $ionicPlatform.on('resume', function(){
      $scope.check_wifi();
});

    var myPopup = {};

    var colsePopup = function() {
        myPopup.close();
    };

    // get gallery
    var venue = wecheckApi.getShareData('venue');
    if (!angular.isArray(venue.gallery)) venue.gallery = [];
    if (!angular.isArray(venue.usergallery)) venue.usergallery = [];

    $scope.gallery = [];
    $scope.userGallery = [];
    $scope.venue = venue;


    angular.forEach(venue.gallery, function (url) {
        $scope.gallery.push({
            src: url,
            sub: ''
        });
    });

    angular.forEach(venue.usergallery, function (url) {
        $scope.userGallery.push({
            src: url,
            sub: ''
        });
    });

    $scope.uploadUserPicture = function (picData) {
        wecheckApi.uploadUserGallery(venue.id, picData).success(function(res) {
            $scope.userGallery = [];
            angular.forEach(res, function (url) {
                $scope.userGallery.push({
                    src: url,
                    sub: ''
                });
            });
        }).error(function () {
            $ionicLoading.hide();
        })
    }

    $scope.showPopup = function() {

        swal({
			title: "Pick Image",
			text: "Selcet where you want to pick the image",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#800000",
			confirmButtonText: "Take Image",
			cancelButtonText: "From Gallery",
			closeOnConfirm: true,
			closeOnCancel: true
			},
		function(isConfirm){
		if (isConfirm) {
					 CameraServ.takePicture('camera').then(function(res) {
                        $scope.uploadUserPicture(res);
                    }, function(error) {

                    });
		} else {
					 CameraServ.takePicture('library').then(function(res) {
                            $scope.uploadUserPicture(res);
                        }, function(error) {
                            console.error(error);
                        });
	 	}
     });
    };


    $scope.itemAction=function(res)
    {
        console.log(res)
    }

})