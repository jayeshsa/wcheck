"use strict"
angular.module('starter.controllers')
.controller('EventInfoCtrl', function($scope,$ionicPlatform, $ionicModal, $sce, $state, $ionicLoading, $timeout, errorPopup, wecheckApi) {
$ionicPlatform.on('resume', function(){
      $scope.check_wifi();
});

  
    var event = wecheckApi.getShareData('event');

    $scope.event = event;

    // get view details
    wecheckApi.getEventDetail(event.id).success(function (res) {
        angular.extend(event, res);

        // check if this venue is opened
        var date = new Date();
        var currentTime = date.getHours() * 60 + date.getMinutes();
        if (event.days && event.from_time && event.to_time) {
            var opendWeeks = event.days.split(',');
            var fromTime = event.from_time.split(':');
            var toTime = event.to_time.split(':');

            fromTime = fromTime.length > 1 ? fromTime[0] * 60 + fromTime[1] * 1 : fromTime[0] * 1;
            toTime = toTime.length > 1 ? toTime[0] * 60 + toTime[1] * 1 : toTime[0] * 1;

            if (opendWeeks.indexOf(date.getDay() + '') != -1 &&
                currentTime >= fromTime && currentTime <= toTime) {
                event.isOpened = true;
            } else {
                event.isOpened = false;
            }
        }
    });

    $scope.favoriteEvent = function($event, event) {
        wecheckApi.favoriteEvent(event.id, !event.favorited).success(function (res) {
            event.favorited = !event.favorited;
        });

        $event.stopPropagation();
        $event.preventDefault();
    };

    $scope.subscribeEvent = function($event) {
        wecheckApi.subscribeEvent(event.id, !event.subscribed).success(function (res) {
            event.subscribed = !event.subscribed;
            errorPopup.toast(event.subscribed ? "Subscribed successfully" : "Unsubscribed successfully");
        });

        if ($event) {
            $event.stopPropagation();
            $event.preventDefault();
        }
    };

    $scope.circularMenuConfig = {
        status:true,
        submenus:[
            {
                menuicon: '',
                img: 'img/venu-menu-navigation.png',
                title : 'Directions'
            },
            {
                menuicon: '',
                img: 'img/venu-menu-phone-call.png',
                title : 'Call'
            },
            {
                menuicon: '',
                img: 'img/venu-menu-share.png',
                title : 'Share'
            },
            {
                menuicon: '',
                img: 'img/icon-like.png',
                title : 'Subscribe'
            },
            {
                menuicon: '',
                img: 'img/venu-menu-uber.png',
                title : 'Uber'
            }
        ]
    };

    $scope.menuFunHandler = function(subMenuindex) {
        switch(subMenuindex){
            case 1:
                wecheckApi.setShareData('venuePos', {
                    latitude: event.latitude,
                    longitude: event.longitude
                });
                $state.go('app.venueDirection');
                break;
            case 2:
                document.location.href="tel:" + event.contact_number;
                break;
            case 3:
                if (!window.plugins || !window.plugins.socialsharing) {
                    errorPopup.toast('Social sharing is not available.');
                    return;
                }

                window.plugins.socialsharing.available(function(isAvailable) {
                    // the boolean is only false on iOS < 6
                    if (isAvailable) {
                        window.plugins.socialsharing.share('https://play.google.com/store/apps/details?id=com.wecheck');// now use any of the share() functions
                    } else {
                        errorPopup.toast('Social sharing is not available.');
                    }
                });

                break;
            case 4:
                $scope.subscribeEvent();
                break;
            case 5:
                var user = wecheckApi.getUser();

                $cordovaGeolocation.getCurrentPosition({
                    timeout: 10000,
                    enableHighAccuracy: false
                }).then(function (position) {
                    window.uber({
                        clientId: "7SycT7sU66JBvrQPI3Dzt8qq4tR81PYB",
                        toLatitude: event.latitude,
                        toLongitude: event.longitude,
                        toAddress: event.address,
                        toNickname: event.title,
                        fromLatitude: position.coords.latitude,
                        fromLongitude: position.coords.longitude,
                        fromNickname: user.name,
                        fromAddress: user.address,
                    }, function(error) {
                        errorPopup.toast(error);
                    });
                }, function (err) {
                    errorPopup.toast("Can't get your position-" + err);
                });
                break;
            default : break;
        }
    };
})