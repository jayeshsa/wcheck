app.controller('NightlifeCtrl', function($scope, $state, $cordovaGeolocation, $ionicLoading, Setting, $rootScope, $cordovaDialogs,
 $filter, wecheckApi, wecheckStorage,$ionicPlatform,errorPopup,$http, $ionicModal) {

    var mainUrl = "http://wecheck.co.za/cms/wecheck_oms/";
    var homedata_loadUrl = "homedata_load.php";

   $scope.loading_homedata=true;
   $scope.loading = true;

   var loadChecklistData = function() {
        $http.get("json/checklist.json")
        .then(function(response) {
            $rootScope.checklistData = response.data;
            $scope.checklistData = $rootScope.checklistData;
            $scope.loading = false;
        })
    }
    loadChecklistData();

    $scope.goToChecklist = function(checklist) {
        $rootScope.selectedCheckList = checklist;
        $state.go('app.Checklist');
    }

    // Opens ionicModal when we add a checklist 
    $ionicModal.fromTemplateUrl('partials/addCategory.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.checklist = {};

    // Add new Check List
    $scope.addChecklist = function() {
        $scope.checklistData.name.push($scope.checklist.name);
        $scope.checklist = {};
        $scope.modal.hide();
    };

   $scope.Home_data = [];
$scope.for_nodata_json = [];
   $scope.filter_for_nightlife = function(str){
        n = str.includes("Nightlife");
         if(n==true){
            $scope.for_nodata_json.push({"a":"b"});
        }
        return n;
   };
console.log($scope.for_nodata_json);

   $scope.load_data=function(loader){
  
       if(loader){$ionicLoading.show({templateUrl: 'templates/spinner.html'});}
          
        wecheckApi.getVenues().success(function(res) {
            console.log(res)
             $scope.refresh_inbox();
            $scope.$broadcast('scroll.refreshComplete');
            $scope.loading_homedata=false;


            $scope.homepageSliders = res.homepage_sliders;
            $scope.Home_data = res.venues;
            if (currentPos.latitude) calculateDistance();
            wecheckApi.setShareData('homepageSliders', $scope.homepageSliders);
             $scope.get_location();

        }).error(function () {
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
            // $scope.loading_homedata=false;
            
        });
   }

    $scope.$on("$ionicView.loaded", function(event, data){
             $ionicPlatform.on('resume', function(){
                $scope.loading_homedata=true;
                $scope.doRefresh_Home(true);
            });
            $scope.doRefresh_Home(true);
    });

    $scope.doRefresh_Home=function(flag)
    {
        $scope.check_wifi();
        $scope.Home_data = []; 
        $scope.load_data(flag);  
    }

 

  


    $scope.data = {};
    $scope.data.sliderOptions = {
        loop: true,
        autoplay: 2000,
        initialSlide: 0,
        direction: 'horizontal', //or vertical
        speed: 500 //0.3s transition
    };

    
    $scope.data.sliderDelegate = null;

    
    $scope.$watch('data.sliderDelegate', function(newVal, oldVal) {
        if (newVal != null) {
            $scope.data.sliderDelegate.on('slideChangeEnd', function() {
                $scope.data.currentPage = $scope.data.sliderDelegate.activeIndex;
                $scope.$apply();
            });
        }
    });


  

    $scope.favoriteVenue = function($event, venue) {
        wecheckApi.favoriteVenue(venue.id, !venue.favorited).success(function (res) {venue.favorited = !venue.favorited;});
        $event.stopPropagation();
        $event.preventDefault();
    };

    $scope.viewVenuInfo = function(venue) {
        wecheckApi.setShareData('venue', venue);
        $state.go('app.venuInfo');
    };

    var currentPos = {};

    var calculateDistance = function () {
        for (var i = 0; i < $scope.Home_data.length; i++) {
            $scope.Home_data[i].distance =
                wecheckApi.calcDistance(currentPos.latitude, currentPos.longitude, $scope.Home_data[i].latitude, $scope.Home_data[i].longitude);
        }
    }

    $scope.get_location=function()
    {

        $cordovaGeolocation.getCurrentPosition({
                timeout: 10000,
                enableHighAccuracy: false
            }).then(function (position) {
                currentPos = position.coords;
                wecheckApi.setShareData('currentPos', {
                    latitude: currentPos.latitude,
                    longitude: currentPos.longitude
                });

                if ($scope.Home_data.length > 0) calculateDistance();

                // get current address
                var geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(currentPos.latitude, currentPos.longitude);

                geocoder.geocode({ location: latlng }, function(data, status) {
                    if (status == google.maps.GeocoderStatus.OK && data[0]) {
                            wecheckApi.getUser().address = data[0].formatted_address;
                    }
                });
            }, function (err) {
                console.log(err);
            });
    }

   

   

    // get new message count
    wecheckStorage.getMessages().then(function (res) {
        var user = wecheckApi.getUser();
        $rootScope.newMailCount = $filter('filter')(res, {is_new: 1}).length;
        $rootScope.newMailCount += user.mail_count * 1;

    });

    // var geocoder = new google.maps.Geocoder();
    // var latlng = new google.maps.LatLng(52.361427, 4.8552631);
    //
    // geocoder.geocode({ location: latlng }, function(data, status) {
    //     console.log(data);
    // });

    // =============================== UMesh ===================================
    // $http.post(mainUrl+homedata_loadUrl).then(function(res){
    //         console.log(res.data);
    //       });



});

