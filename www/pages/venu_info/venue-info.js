app.controller('VenuCtrl', function($scope, $ionicModal,$ionicPlatform, $sce, $state, $ionicLoading, $timeout, $cordovaGeolocation, errorPopup, wecheckApi) {
 $ionicPlatform.on('resume', function(){

      $scope.check_wifi();
});

// pdfDelegate
 
    var venue = wecheckApi.getShareData('venue');

    $scope.venue = venue;

    // get view details
    wecheckApi.getVenueDetail(venue.id).success(function (res) {
        angular.extend(venue, res);

        // check if this venue is opened
        var date = new Date();
        var currentTime = date.getHours() * 60 + date.getMinutes();
        if (venue.days && venue.from_time && venue.to_time) {
            var opendWeeks = venue.days.split(',');
            var fromTime = venue.from_time.split(':');
            var toTime = venue.to_time.split(':');

            fromTime = fromTime.length > 1 ? fromTime[0] * 60 + fromTime[1] * 1 : fromTime[0] * 1;
            toTime = toTime.length > 1 ? toTime[0] * 60 + toTime[1] * 1 : toTime[0] * 1;

            if (opendWeeks.indexOf(date.getDay() + '') != -1 &&
                currentTime >= fromTime && currentTime <= toTime) {
                venue.isOpened = true;
            } else {
                venue.isOpened = false;
            }
        }

        // trust pdf url
        // venue.pdf_url = $sce.trustAsResourceUrl(venue.pdf_url);

        // save detail
        wecheckApi.setShareData('venue', venue);

        $scope.venue = venue;

    });

    $scope.showFabMenu = false;

    $scope.venueFav = false;

    $scope.toggleVenuFav = function($event) {
        $scope.venueFav = !$scope.venueFav;
    };

    $scope.toggleFabMenu = function(e) {
        e.preventDefault();
        $scope.showFabMenu = !$scope.showFabMenu;
    };

    $scope.favoriteVenue = function($event, venue) {
        wecheckApi.favoriteVenue(venue.id, !venue.favorited).success(function (res) {
            venue.favorited = !venue.favorited;
        });

        $event.stopPropagation();
        $event.preventDefault();
    };

    $scope.subscribeVenue = function($event) {
        wecheckApi.subscribeVenue(venue.id, !venue.subscribed).success(function (res) {
            venue.subscribed = !venue.subscribed;
            errorPopup.toast(venue.subscribed ? "Subscribed successfully" : "Unsubscribed successfully");
        });

        if ($event) {
            $event.stopPropagation();
            $event.preventDefault();
        }
    };

    $scope.circularMenuConfig = {
        status:true,
        submenus:[
            {
                menuicon: '',
                img: 'img/venu-menu-navigation.png',
                title : 'Directions'
            },
            {
                menuicon: '',
                img: 'img/venu-menu-phone-call.png',
                title : 'Call'
            },
            {
                menuicon: '',
                img: 'img/venu-menu-share.png',
                title : 'Share'
            },
            {
                menuicon: '',
                img: 'img/icon-like.png',
                title : 'Subscribe'
            },
            {
                menuicon: '',
                img: 'img/venu-menu-uber.png',
                title : 'Uber'
            }
        ]
    };

    $scope.menuFunHandler = function(subMenuindex) {
        switch(subMenuindex){
            case 1:
                wecheckApi.setShareData('venuePos', {
                    latitude: venue.latitude,
                    longitude: venue.longitude
                });
                $state.go('app.venueDirection');
                break;
            case 2:
                document.location.href="tel:" + venue.contact_number;
                break;
            case 3:
                if (!window.plugins || !window.plugins.socialsharing) {
                    errorPopup.toast('Social sharing is not available.');
                    return;
                }

                window.plugins.socialsharing.available(function(isAvailable) {
                    // the boolean is only false on iOS < 6
                    if (isAvailable) {
                        window.plugins.socialsharing.share("https://play.google.com/store/apps/details?id=com.wecheck");// now use any of the share() functions
                        // window.plugins.socialsharing.share($scope.venue.share_url);// now use any of the share() functions
                    } else {
                        errorPopup.toast('Social sharing is not available.');
                    }
                });

                break;
            case 4:
                $scope.subscribeVenue();
                break;
            case 5:
                var user = wecheckApi.getUser();

                $cordovaGeolocation.getCurrentPosition({
                    timeout: 10000,
                    enableHighAccuracy: false
                }).then(function (position) {
                    window.uber({
                        clientId: "7SycT7sU66JBvrQPI3Dzt8qq4tR81PYB",
                        toLatitude: venue.latitude,
                        toLongitude: venue.longitude,
                        toAddress: venue.address,
                        toNickname: venue.title,
                        fromLatitude: position.coords.latitude,
                        fromLongitude: position.coords.longitude,
                        fromNickname: user.name,
                        fromAddress: user.address,
                    }, function(error) {
                        errorPopup.toast(error);
                    });
                }, function (err) {
                    errorPopup.toast("Can't get your position-" + err);
                });
                break;
            default : break;
        }
    };

    $scope.review = function () {
        $ionicModal.fromTemplateUrl('templates/review-modal.html',function(modal) {
            $scope.modalCtrl = modal;
            $scope.venue = venue;
        }, {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            modal.show();
        });
    }


})