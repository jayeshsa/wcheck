'use strict'
angular.module('starter.controllers')
.controller('RewardsCtrl', function($scope,$ionicPlatform, $ionicPopup,errorPopup, $rootScope, $http, StoreRewardsOfflineService, $ionicLoading, $q, $timeout, wecheckApi) {
$ionicPlatform.on('resume', function(){
      $scope.check_wifi();
});

  
    var user = wecheckApi.getUser();

    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };

    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };

    var deferred = $q.defer();
    var getVenuesFromServer = function() {
        $scope.venues = [];
        var venue_list_url = 'http://wecheck.co.za/rewards/index.php/api/get_venue_list?' +
            "user_id=" + user.id;
        $http.post(venue_list_url).then(function(response) {
            for (var i = 0; i < response.data.length; i++) {
                $scope.venues.push(response.data[i]);
                var reward_url = 'http://wecheck.co.za/rewards/index.php/api/get_reward_data?' +
                    "user_id=" + response.data[i].user_id +
                    "&merchant_id=" + response.data[i].merchant_id;
                (function executeReqIIFE(index, length) {
                    var count = 0;
                    $http.post(reward_url).then(function(rewards_response) {
                        console.warn(rewards_response);
                        $scope.venues[index].rewards = rewards_response.data.rewards;
                        if (length == ++count) {
                            deferred.resolve();
                            StoreRewardsOfflineService.updateRewards($scope.venues);
                        }
                    }, function() {

                    });
                })(i, response.data.length);
            }
        }, function(e) {
            console.log(e);
            errorPopup.showPopup("Error!!","Error Fetching Records from server, Please check your network connectivity and try agian later.","error")
            $scope.venues = StoreRewardsOfflineService.getRewards();
            deferred.resolve();
        });
        return deferred.promise;
    };

    if (!$scope.venues)
        getVenuesFromServer();

    $scope.$on("QR_SCAN_SUCCESS", function(e, data) {

        var timeout = null;
        addQRResult();

        function addQRResult() {
            if (!deferred.promise.$$state.status) {
                if (timeout)
                    $timeout.cancel(timeout);
                timeout = $timeout(addQRResult, 200);
                return;
            }

            $timeout.cancel(timeout);
            var newVenue = data.data,
                isVenueAlreadyPresent = false;
            $scope.venues.forEach(function(venue) {
                if (venue.venue_name == newVenue.venue_name) {
                    isVenueAlreadyPresent = true;
                    venue.points_balance = newVenue.points_balance;
                    venue.rewards = newVenue.rewards;
                }
            });
            if (!isVenueAlreadyPresent) {
                $scope.venues.push(newVenue);
            }

            StoreRewardsOfflineService.updateRewards($scope.venues);
        }
    });

    $scope.rewardClicked = function(reward, venue) {

        
        $scope.reward = {};
        $scope.reward.id = reward._id;
        $scope.reward.merchant_id = reward.merchant_id;
        $scope.reward.title = reward.title;
        $scope.reward.venue_name = venue.venue_name;
        
        $ionicPopup.show({
            template: '<input type="text" ng-model="reward.staffcode" placeholder="Please pass your device to a staff member" />',
            title: "Enter Staff Code",
            scope: $scope,
            cssClass: 'codereward_dialog',
            buttons: [{
                text: 'Cancel'
            },
                {
                    text: "OK",
                    type: "button-positive",
                    onTap: function(e) {
                        if (!$scope.reward.staffcode) {
                            //don't allow the user to close unless he enters staff code
                            e.preventDefault();
                        } else {
                            var deduction = CallClaimRewardService();
                        }
                    }
                }
            ]
        });
    };
    

    function CallClaimRewardService() {
        console.info($scope.reward);
        console.info(user.id);
        var url = 'http://wecheck.co.za/rewards/index.php/api/claim_reward?' +
            "user_id=" + user.id +
            "&merchant_id=" + $scope.reward.merchant_id +
            "&reward_id=" + $scope.reward.id +
            "&staff_code=" + $scope.reward.staffcode;

        $http.post(url).then(function(response) {
            console.info(response);
            if (response.data.success) {
                errorPopup.showPopup('(' + $scope.reward.venue_name + ') Congratulations','You have claimed a ' + $scope.reward.title,"info")
                updateRewardBalance(response.data.points_balance);
            } else {
                var message = "";
                switch (response.data.failed_reason) {
                    case 1:
                        message = "Invalid Staff Code";
                        break;
                    case 2:
                        message = "Points Balance is not enough";
                        break;
                    default:
                        message = "Please check your connection and try again!"
                }
                errorPopup.showPopup('(' + $scope.reward.venue_name + ') Error',message,"error")
            
            }
        }, function(error) {
            console.error(error);
                errorPopup.showPopup('Error!!',message,"Invalid staff code, Please enter a valid staff code.")
        })
    }

    function updateRewardBalance(points_balance) {
        $scope.venues.forEach(function(venue) {
            if (venue.merchant_id == $scope.reward.merchant_id) {
                venue.rewards.forEach(function(reward) {
                    if (reward._id == $scope.reward.id) {
                        venue.points_balance = reward.points_balance = points_balance;
                    }
                })
            }
        });
        StoreRewardsOfflineService.updateRewards($scope.venues);
    }
})