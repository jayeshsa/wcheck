'use strict';
angular.module('starter').controller('signupCtrl',
function($scope, CameraServ, $cordovaNetwork, errorPopup, $location, $cordovaFileTransfer, serverUrl,
		 $http, $base64, $ionicPlatform, Setting, $state, wecheckApi) {
$ionicPlatform.on('resume', function(){
      $scope.check_wifi();
});

	var myPopup = {};
	$scope.signup = {};

	$scope.showPopup = function() {

		swal({
			title: "Pick Image",
			text: "Selcet where you want to pick the image",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#800000",
			confirmButtonText: "Take Image",
			cancelButtonText: "From Gallery",
			closeOnConfirm: true,
			closeOnCancel: true
			},
		function(isConfirm){
			console.log(isConfirm);
		if (isConfirm) {
			CameraServ.takePicture('camera').then(function(res) {
				$scope.myimage = res;
			}, function(error) {

			});
		} else {
		   CameraServ.takePicture('library').then(function(res) {
				$scope.myimage = res;
			}, function(error) {
				console.error(error);
			});
		}
		});



		
	};

	$scope.fnRegister = function(info) {
		// if ($cordovaNetwork.isOffline()) {
		// 	errorPopup.noInternet();
		// 	return false;
		// }

		// if (!$scope.myimage) {
		// 	errorPopup.showPopup('Alert!!','Please select the profile avatar',"info");
		// 	return false;
		// };

		wecheckApi.signUp({
            "password": info.password,
            "email": info.email,
            "name": info.name,
            "mobile": info.mobile,
            "profile_pic": $scope.myimage
		}).success(function (res) {
			wecheckApi.setUser(res);
            Setting.setData('api_key', res.api_key);
            Setting.setData('logged_in', true);
            wecheckApi.setApiKey(res.api_key);
            $state.go('app.home');
		}).error(function (res) {

		});
	};
});