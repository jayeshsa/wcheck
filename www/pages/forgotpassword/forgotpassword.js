"use strict"
angular.module('starter.controllers')
.controller('forgotPasswordCtrl', function($scope,$ionicPlatform, $sce, $state, errorPopup, wecheckApi) {
$ionicPlatform.on('resume', function(){
      $scope.check_wifi();
});

    $scope.resetPassword = function (email) {
        wecheckApi.resetPassword(email).success(function (res) {
            errorPopup.toast(res.message);
            console.log(res.message);
            if (res.success) {
                $state.go('default.login');
            }
        });
    };
})
