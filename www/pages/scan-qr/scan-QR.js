'use strict'
angular.module('starter.controllers')
.controller('ScanQRCtrl', function($scope, $state, $http, $rootScope, $ionicLoading, $timeout, $ionicPlatform, wecheckApi,errorPopup,$cordovaBarcodeScanner) {
$ionicPlatform.on('resume', function(){
      $scope.check_wifi();
});

        var user = wecheckApi.getUser();

        // We will scan our barcode here
        document.addEventListener("deviceready", function() {
            scanQRCode();
        }, false);

        function unknownQRCode() {

              swal({
                        title: "Error!!",
                        text: "Unknown QR Code! Try Again ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#800000",
                        confirmButtonText: "Rescan",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                    if (isConfirm) {
                        scanQRCode();
                    } else {
                         $state.go("app.home");
                    }
                 });


        }

        function scanQRCode() {
            cordova.plugins.diagnostic.requestCameraAuthorization({
                successCallback: function(status){
                    console.log(status)
                                if(status == cordova.plugins.diagnostic.permissionStatus.GRANTED )
                                {
                                    // cordova.plugins.barcodeScanner.scan(function(barcodeData) {
                                        $cordovaBarcodeScanner
                                        .scan()
                                        .then(function(barcodeData) {
                            // Success
                            try {
                                barcodeData = JSON.parse(barcodeData.text);
                            } catch (e) {
                                unknownQRCode();
                                return;
                            }
                            var keys = Object.keys(barcodeData);
                            if (!keys.contains('id') || !keys.contains("name") || !keys.contains("email") && !keys.contains("venue")) {
                                unknownQRCode();
                                return;
                            }

                            // Get Points earned
                            var url = "http://wecheck.co.za/rewards/index.php/api/scan_qr_code?" +
                                "user_id=" + user.id + "&" +
                                "user_name=" + user.name + "&" +
                                "merchant_id=" + barcodeData.id;


                            var headers = {
                                'Access-Control-Allow-Origin': '*',
                                'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
                                'Content-Type': 'application/json',
                                'Accept': 'application/json'
                            };

                            $http({
                                method: "post",
                                headers: headers,
                                url: url
                            }).then(function(response) {

                                swal({
                                    title:  '(' + response.data.venue_name + ') Congratulations!',
                                    text: 'You have earned ' + response.data.earned_points + ' points. \nPlease scan QR code again' +(response.data.repeat_hours > 0 ? " after " + response.data.repeat_hours + " hours" : "") +" to earn more points.",
                                    type: "warning",
                                    confirmButtonColor: "#800000",
                                    confirmButtonText: "I Got It!",
                                    closeOnConfirm: true
                                },
                                function(){
                                $state.go("app.myRewards");

                                    $timeout(function() {
                                        $rootScope.$broadcast("QR_SCAN_SUCCESS", {
                                            data: response.data
                                        });
                                    }, 100);
                                });

                            
                            }, function(error) {
                                errorPopup.noInternet()
                            });
                        }, function(error) {
                            errorPopup.showPopup('Error!!',message,"Please Scanning valid Barcode.")
                        }, {
                            "resultDisplayDuration": 0
                        });   
                    }
                    else
                    {
                            errorPopup.showPopup("Alert!!","Wecheck need camera permission for QR-Scaning, Please Enable the Camera permission in Setting.","error");
                    }
                },
                errorCallback: function(error){
                
                }
            });
           
        }
    }
);