angular.module('starter.controllers')
.controller('MapCtrl', function($scope,$ionicPlatform, $state, $cordovaGeolocation, uiGmapIsReady, uiGmapGoogleMapApi, wecheckApi,Loading) {
 $ionicPlatform.on('resume', function(){
      $scope.check_wifi();
       $scope.user = wecheckApi.getUser();
});

var profileimgUrl = $scope.user.profile_image_url;
var profileimgUrl2 = profileimgUrl.replace("?type=large", "");
console.log(profileimgUrl2)

    $scope.userPos = {
        latitude: -33.98457440000001,
        longitude: 18.50
    };

    $scope.map = {
        center: $scope.userPos,
        zoom: 6,
        control: {}
    };

    $scope.options = {
        scrollwheel: false,
        mapTypeControl: false
    };

    $scope.userMarkerOption = {
        icon: 'img/50x50.png'
    };

    $scope.venueOption = {
        icon: 'img/accomo_60x60.png'
    };

    $scope.eventOption = {
        icon: 'img/cutlery_60x60.png'
    };

    $scope.getmark = function(vm){
        console.log(vm)
    //    return $scope.venueOption;
    };

    // $scope.eventOption = {}

    // $scope.events = $scope.venues = [];



    $scope.windowOptions = {
        boxClass: "infobox",
        boxStyle: {
            backgroundColor: "#FFFFFF",
            border: "1px solid #800000",
            width: "200px",
            height: "100px"
        },
        disableAutoPan: false,
        maxWidth: 0,
        zIndex: null,
        isHidden: false,
        pane: "floatPane",
        enableEventPropagation: false,
        visible: true
    };

   

    // // center map
    uiGmapIsReady.promise().then(function (map_instances) {
        $scope.mapRef = $scope.map.control.getGMap();
        return uiGmapGoogleMapApi;
    }).then(function (maps) {
        return $cordovaGeolocation.getCurrentPosition();
    }).then(function (position) {
        $scope.userPos.latitude=position.coords.latitude;
        $scope.userPos.longitude=position.coords.longitude;
        $scope.map.center = $scope.userPos;
        $scope.getDirections($scope.userPos.latitude, $scope.userPos.longitude);
        // var marker = new google.maps.Marker({
        //     position: {lat: $scope.userPos.latitude, lng: $scope.userPos.longitude},
        //     map: $scope.map ,
        //     title: '',
        //     label:"",
        //         icon: 'img/50x50.png'
        // });
        var myLatlng = new google.maps.LatLng($scope.userPos.latitude, $scope.userPos.longitude);
                    var marker = new RichMarker({
                    position: myLatlng,
                    map: $scope.map,
                    // content: '<div id="talkbubble">' +
                    //     '<div><img class="buble_img" src="'+profileimgUrl2+'"/></div></div>'
                    content: '<div class="pin" >' +
                        '<div><img class="pin_img" src="img/user-marker.png"/></div></div>'
                    });
        marker.setMap($scope.mapRef);

       // get_data();
    }, function (err) {
      //  get_data();
        
    });
    // var posOptions = {timeout: 10000, enableHighAccuracy: false};
    // $scope.getlocation = function(){
    //       $cordovaGeolocation
    //         .getCurrentPosition(posOptions)
    //         .then(function (position) {
    //           var lat  = position.coords.latitude
    //           var long = position.coords.longitude
    //           localStorage.setItem('userlat', lat);
    //           localStorage.setItem('userlng', long);
    //           initMap();
    //         }, function(err) {
    //           // error
    //            var lat  =  -33.98457440000001;
    //            var long =  18.50;
    //            localStorage.setItem('userlat', lat);
    //            localStorage.setItem('userlng', long);
    //         });

    // }
    // $scope.getlocation();


    
    // function initMap() {
    //     $scope.venueLat = localStorage.getItem('venueLat');
    //     $scope.venueLong = localStorage.getItem('venueLong');

    //     var userlat = localStorage.getItem('userlat');
    //     var userlng = localStorage.getItem('userlng');
    //     var directionsService = new google.maps.DirectionsService;
    //     var directionsDisplay = new google.maps.DirectionsRenderer;
    //     var map = new google.maps.Map(document.getElementById('direction_map'), {
    //       zoom: 8,
    //       center: {lat: Number(userlat), lng: Number(userlng)},
    //       mapTypeControl: false,
    //       mapTypeControlOptions: {
    //         style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
    //         mapTypeIds: ['roadmap', 'terrain']
    //       }
    //     });
    //     directionsDisplay.setMap(map);
    //      // dis_calculate();
         
    //      setTimeout(function() { calculateAndDisplayRoute(directionsService, directionsDisplay); }, 1000);

    //   }

    // function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    //     $scope.venueLat = localStorage.getItem('venueLat');
    //     $scope.venueLong = localStorage.getItem('venueLong');

    //     var userlat = localStorage.getItem('userlat');
    //     var userlng = localStorage.getItem('userlng');
    //     directionsService.route({
    //       // origin: start,
    //       // destination: end,
    //       origin:new google.maps.LatLng(Number(userlat),Number(userlng)),
    //       destination:new google.maps.LatLng(Number($scope.venueLat),Number($scope.venueLong)),
    //       travelMode: google.maps.DirectionsTravelMode.DRIVING,

    //     }, function(response, status) {
    //       if (status === 'OK') {
    //         directionsDisplay.setDirections(response);
    //       } else {
    //         console.log('Directions request failed due to ' + status);
    //       }
    //     });
    //     dis_calculate();

    // }

    // function dis_calculate()
    //     {
    //     $scope.venueLat = localStorage.getItem('venueLat');
    //     $scope.venueLong = localStorage.getItem('venueLong');

    //     var userlat = localStorage.getItem('userlat');
    //     var userlng = localStorage.getItem('userlng');
    //    var service = new google.maps.DistanceMatrixService;
    //          service.getDistanceMatrix({
    //            origins: [{lat: Number(userlat), lng: Number(userlng)}],
    //            destinations: [{lat: Number($scope.venueLat), lng: Number($scope.venueLong)}],
    //            travelMode: google.maps.TravelMode.DRIVING,
    //            unitSystem: google.maps.UnitSystem.METRIC,
    //            avoidHighways: false,
    //            avoidTolls: false
    //          }, function(response, status) {
    //             console.log(response);
    //            if (status !== google.maps.DistanceMatrixStatus.OK) {
    //              // alert('Error was: ' + status);
    //            } else {
    //               $scope.valuedistance = response.rows[0].elements[0].distance.text;
    //               localStorage.setItem("Distance", $scope.valuedistance);  
    //               $scope.valueTime= response.rows[0].elements[0].duration.text;
    //            }
    //        });
    //     }




    //     setTimeout(function() {  dis_calculate(); }, 100);
    
 $scope.getDirections = function (userlat, userlong) {
        $scope.venueLat = localStorage.getItem('venueLat');
        $scope.venueLong = localStorage.getItem('venueLong');
        // var userlat = localStorage.getItem('userlat');
        // var userlng = localStorage.getItem('userlng');
        uiGmapGoogleMapApi.then(function (maps) {
            var directionsService = new maps.DirectionsService();
            var originStr = userlat + ", " + userlong;
            var destStr = $scope.venueLat + ", " + $scope.venueLong;
            var request = {
                origin: originStr,
                destination: destStr,
                travelMode: maps.TravelMode['DRIVING'],
                optimizeWaypoints: true
            };

            directionsService.route(request, function (response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    $scope.directionsDisplay.setMap(null);
                    $scope.directionsDisplay.setMap($scope.mapRef);
                    $scope.directionsDisplay.setDirections(response);

                } else {

                   
                    var flightPlanCoordinates = [
                        new google.maps.LatLng(userlat, userlong),
                        new google.maps.LatLng($scope.venueLat, $scope.venueLong)
                    ];
                    var flightPath = new google.maps.Polyline({
                        path: flightPlanCoordinates,
                        strokeColor: "#800000",
                        strokeOpacity: 1.0,
                        strokeWeight: 3
                    });

                   
                     var marker = new google.maps.Marker({
                        position: {lat: userlat, lng: userlong},
                        map: $scope.map ,
                        title: 'Current location',
                        label:"",
                        icon: 'img/user-marker.png'
                    });
                    marker.setMap($scope.mapRef);

                     var marker1 = new google.maps.Marker({
                        position: {lat: $scope.venueLat, lng: $scope.venueLong},
                        map: $scope.map ,
                        title: 'Destination location',
                        label:"B",
                        icon: 'img/custom_pin.png'
                    });
                    marker1.setMap($scope.mapRef);


                     flightPath.setMap($scope.mapRef);

                    errorPopup.showPopup("Alert!!","No Directions avilable for your current location","info")
                    console.log('Directions request failed due to ' + status);
                }
            });


        });
    }


   

    function get_data()
    {
        wecheckApi.getVenues().success(function (res) {
           
               console.log(res.venues)
               $scope.getDirections($scope.userPos.latitude, $scope.userPos.longitude);
                // $scope.venues = res.venues;
                // $scope.venues = res.venues.substring(0,70);
                // // $scope.getDirections($scope.venues, $scope.userPos);
                // for(var i=0;i<$scope.venues.length;i++)
                // {
                //     console.log($scope.venues[i])
                //     if($scope.filter_for_restaurant($scope.venues[i].category) == "Accommodation"){
                //         console.log("accomo come")
                //            var marker = new google.maps.Marker({
                //                 position: {lat: Number($scope.venues[i].latitude), lng: Number($scope.venues[i].longitude)},
                //                 map: $scope.map ,
                //                 title: '',
                //                 label:"",
                //                 icon: 'img/sm_bed.png'
                //             });
                //             marker.setMap($scope.mapRef);
                //     }
                //     else if($scope.filter_for_restaurant($scope.venues[i].category) == "Restaurants"){
                //              console.log("Restaurants come")
                //            var marker = new google.maps.Marker({
                //                 position: {lat: Number($scope.venues[i].latitude), lng: Number($scope.venues[i].longitude)},
                //                 map: $scope.map ,
                //                 title: '',
                //                 label:"",
                //                 icon: 'img/sm_cutlery.png'
                //             });
                //             marker.setMap($scope.mapRef);
                //     }
                //     else if($scope.filter_for_restaurant($scope.venues[i].category) == "Nightlife"){
                //              console.log("Nightlife come")
                //            var marker = new google.maps.Marker({
                //                 position: {lat: Number($scope.venues[i].latitude), lng: Number($scope.venues[i].longitude)},
                //                 map: $scope.map ,
                //                 title: '',
                //                 label:"",
                //                 icon: 'img/nightlife_pin.png'
                //             });
                //             marker.setMap($scope.mapRef);
                //     }

                //     //    var marker = new google.maps.Marker({
                //     //     position: {lat: Number($scope.venues[i].latitude), lng: Number($scope.venues[i].longitude)},
                //     //     map: $scope.map ,
                //     //     title: '',
                //     //     label:"",
                //     //     icon: 'img/sm_bed.png'
                //     // });
                //     // marker.setMap($scope.mapRef);
                // }
            });

            // wecheckApi.getEvents().success(function (res) {
            //     console.log(res);
            //     $scope.events = res;

            // });
    }


// ================================= Set my current location ============================

$scope.Where_is_me = function(){
    console.log("hih");
    $scope.centerObj = {
        latitude: localStorage.getItem('userlat'),
        longitude : localStorage.getItem('userlng')
    };

    google.maps.event.addDomListener(window, 'resize', function() {
        map.setCenter($scope.centerObj);
    });
    //  uiGmapIsReady.promise().then(function (map_instances) {
    //     $scope.mapRef = $scope.map.control.getGMap();
    //     return uiGmapGoogleMapApi;
    // }).then(function (maps) {
    //     return $cordovaGeolocation.getCurrentPosition();
    // }).then(function (position) {
    //     $scope.userPos.latitude=position.coords.latitude;
    //     $scope.userPos.longitude=position.coords.longitude;
    //     $scope.map.center = $scope.userPos;
    //     console.log($scope.userPos);
    //     // var marker = new google.maps.Marker({
    //     //     position: {lat: $scope.userPos.latitude, lng: $scope.userPos.longitude},
    //     //     map: $scope.map ,
    //     //     title: '',
    //     //     label:"",
    //     //         icon: 'img/50x50.png'
    //     // });
    //      var myLatlng = new google.maps.LatLng($scope.userPos.latitude, $scope.userPos.longitude);
    //                 var marker = new RichMarker({
    //                 position: myLatlng,
    //                 map: $scope.map,
    //                 // content: '<div id="talkbubble">' +
    //                 //     '<div><img class="buble_img" src="'+profileimgUrl2+'"/></div></div>'
    //                 content: '<div class="pin" >' +
    //                     '<div><img class="pin_img" src="'+profileimgUrl2+'"/></div></div>'
    //                 });
    //     marker.setMap($scope.mapRef);

    //    // get_data();
    // }, function (err) {
    //    // get_data();
        
    // });

};




// $scope.filter_for_restaurant = function(str){
//         if(str.includes("Restaurants")){return "Restaurants";}
//         else if(str.includes("Accommodation")){return "Accommodation";}
//         else if(str.includes("Nightlife")){return "Nightlife";}
//    };



//  $(document).on("click", ".read_more", function() {
            
//             $(".user-details-after").show();
//             $(".user-details").hide();
//         });
//  $(document).on("click", ".hidemore", function() {
           
//             $(".user-details-after").hide();
//             $(".user-details").show();
//         });

// $(document).ready(function(){
//     $(".user-details-after").hide();
// })
});