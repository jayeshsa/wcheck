angular.module('starter')
.controller('ProfileCtrl', function($scope,$ionicPlatform, Setting, CameraServ, $cordovaNetwork, errorPopup, Loginservice, $ionicLoading, $rootScope, wecheckApi) {
$ionicPlatform.on('resume', function(){
      $scope.check_wifi();
});


  
    $scope.info = {};
    wecheckApi.getUserInfo().success(function (res) {
        $scope.info = res;
    });

    function convertImgToBase64(url, callback, outputFormat) {
        var canvas = document.createElement('CANVAS'),
            ctx = canvas.getContext('2d'),
            img = new Image;
        img.crossOrigin = 'Anonymous';
        img.onload = function() {
            var dataURL;
            canvas.height = img.height;
            canvas.width = img.width;
            ctx.drawImage(img, 0, 0);
            dataURL = canvas.toDataURL(outputFormat);
            callback.call(this, dataURL);
            canvas = null;
        };
        img.src = url;
    }

    $scope.editimage = function() {
        if ($cordovaNetwork.isOffline()) {
            errorPopup.noInternet();
            return false;
        }


		swal({
                title: "Pick Image",
                text: "Selcet where you want to pick the image",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#800000",
                confirmButtonText: "Take Image",
                cancelButtonText: "From Gallery",
                closeOnConfirm: true,
                closeOnCancel: true
                },
            function(isConfirm){
            if (isConfirm) {
                CameraServ.takePicture('camera').then(function(res) {
                        wecheckApi.updateProfileImage(res).success(function () {
                            debugger
                            wecheckApi.getUser().profile_image_url = res.profile_image_url;
                        });
                    }, function(error) {

                    });
            } else {
                CameraServ.takePicture('library').then(function(res) {
                            wecheckApi.updateProfileImage(res).success(function () {
                                debugger
                                wecheckApi.getUser().profile_image_url = res.profile_image_url;
                            });
                        }, function(error) {
                            console.error(error);
                        });
            }
            });
        };

   
})
