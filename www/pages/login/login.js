'use strict';
angular.module('starter').controller('LoginCtrl',function ($scope, $rootScope, $timeout, $location, $state, $ionicLoading,  PopupService,
              $cordovaNetwork, errorPopup, wecheckApi, $cordovaOauth, $ionicPlatform, $cordovaFileTransfer, Setting, $http, ngFB,Loading,
              $ionicFacebookAuth, $ionicUser, $ionicModal, CameraServ, serverUrl, $base64) {
 $ionicPlatform.on('resume', function(){
      $scope.check_wifi();
    });
	var myPopup = {};
	$scope.signup = {};
	
	$scope.showPopup = function() {
		swal({
			title: "Pick Image",
			text: "Selcet where you want to pick the image",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#800000",
			confirmButtonText: "Take Image",
			cancelButtonText: "From Gallery",
			closeOnConfirm: true,
			closeOnCancel: true
			},
		function(isConfirm){
      console.log(isConfirm);
			if (isConfirm) {
				CameraServ.takePicture('camera').then(function(res) {
					$scope.myimage = res;
				}, function(error) {
				});
			} else {
				CameraServ.takePicture('library').then(function(res) {
						$scope.myimage = res;
				}, function(error) {
						console.error(error);
				});
			}
		});
	};
	
	$ionicModal.fromTemplateUrl('pages/login/modal/login.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(login) {
    $scope.login = login;
  });
  $scope.openModallogin = function() {
    $scope.login.show();
  };
  $scope.closeModallogin = function() {
    $scope.login.hide();
  };
  
  $ionicModal.fromTemplateUrl('pages/login/modal/signup.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(signup) {
    $scope.signup = signup;
  });
  $scope.openModalsignup = function() {
    $scope.signup.show();
  };
  $scope.closeModalsignup = function() {
    $scope.signup.hide();
  };
  $scope.gotoForgot = function(){
    $scope.login.hide();
    $state.go('forgotpassword');
  }
    $scope.doLogin = function(user) {

        // if ($cordovaNetwork.isOffline()) {
        //    errorPopup.toast('Please check your network connection');
        //    return false;
        // }
        wecheckApi.login(user).then(function(res) {
            $rootScope.user = res;
            wecheckApi.setUser(res);
            Setting.setData('logged_in', true);
            $scope.login.hide();
            $state.go('app.home');
        }, function(error) {
            errorPopup.toast(error.data);
        });
    };
	
	$scope.fnRegister = function(info) {
		if ($cordovaNetwork.isOffline()) {
			errorPopup.noInternet();
			return false;
		}

		if (!$scope.myimage) {
			errorPopup.showPopup('Alert!!','Please select the profile avatar',"info");
			return false;
		};

		wecheckApi.signUp({
            "password": info.password,
            "email": info.email,
            "name": info.name,
            "mobile": info.mobile,
            "profile_pic": $scope.myimage
		}).success(function (res) {
			wecheckApi.setUser(res);
            // Setting.setData('api_key', res.api_key);
            Setting.setData('logged_in', true);
            // wecheckApi.setApiKey(res.api_key);
            $state.go('app.home');
		}).error(function (res) {

		});
	};

    $scope.facebookSignIn = function() {
       if ($cordovaNetwork.isOffline()) {
           errorPopup.noInternet();
           return false;
       }

        // $cordovaOauth.facebook("541176595984451", ["email"]).then(function (res) {
        //   console.log('facebook success');
        // }, function (error) {
        //   console.log('facebook error');
        // });
        
        $ionicFacebookAuth.login().then(function(){

           $scope.facebookemail = $ionicUser.social.facebook.data.email;
           $scope.facebookfullname = $ionicUser.social.facebook.data.full_name;
           $scope.facebookuid = $ionicUser.social.facebook.uid;
           $scope.facebookimage = "https://graph.facebook.com/" + $ionicUser.social.facebook.uid + "/picture?type=large";
           

           
           
          wecheckApi.socialLogin({
                       email: $scope.facebookemail,
                       name: $scope.facebookfullname,
                       profile_image_url: "http://graph.facebook.com/" + $scope.facebookuid + "/picture?width=270&height=270"
                   }).then(function(res) {
                        $rootScope.user = res;
                        $rootScope.user.profile_image_url="https://graph.facebook.com/" + $ionicUser.social.facebook.uid + "/picture?type=large";
                        wecheckApi.setUser(res);
                        Setting.setData('logged_in', true);
                        $state.go('app.home');

                    }, function(error) {
                        errorPopup.toast(error.data);
                    });
                    
      
     
         }, function (fail) {
        //fail get profile info
        console.log('ionicFacebookAuth info fail', fail);
        });
  
    };
	
	$scope.gotohome=function(){
		Setting.setData('logged_in', true);
		$scope.login.hide();
		$state.go('app.home');
	}
	
    $scope.fnwechatlogin = function() {
           window.plugins.googleplus.login({
                scopes: '', 
                webClientId: '981691518423-2sp3coengfcoqjd5kj0jtdhero75rcln.apps.googleusercontent.com',
                // webClientId: '981691518423-jan6gn7i4b8no76vocj5abgo871fnsn3.apps.googleusercontent.com',
                offline: true 
          },function (user_data) {
            console.log(user_data)
               $scope.gdisplayName=user_data.displayName;
               $scope.gemail=user_data.email;
               $scope.gimageUrl=user_data.imageUrl+"?sz=256";
               $scope.guserId=user_data.userId;
            
            
                 wecheckApi.socialLogin({
                       email: $scope.gemail,
                       name: $scope.gdisplayName,
                       profile_image_url: $scope.gimageUrl
                   }).then(function(res) {
                       console.log(res)
                        $rootScope.user = res;
                        $rootScope.user.profile_image_url=$scope.gimageUrl;
                        wecheckApi.setUser(res);
                        Setting.setData('logged_in', true);
                        $state.go('app.home');

                    }, function(error) {
                        errorPopup.toast(error.data);
                    });

            },function (msg) {
               
            });    
    };
});
