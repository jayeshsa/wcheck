app.controller('ChecklistCtrl', function(
    $scope,
    $rootScope,
    $cordovaDialogs,
    $filter,
    $ionicModal,
    $stateParams) {

    $scope.checkListName = $rootScope.selectedCheckList;
    $scope.checklistItems = [];

    angular.forEach($rootScope.checklistData[$rootScope.selectedCheckList], function(value, key) {
        var obj = {
            name: value
        };
        $scope.checklistItems.push(obj);
    });

    // Opens ionicModal when we add a checklist 
    $ionicModal.fromTemplateUrl('partials/addChecklist.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.checklist = {};

    // Add new Check List
    $scope.addChecklist = function() {
        $scope.checklistItems.push($scope.checklist);
        $scope.checklist = {};
        $scope.modal.hide();
    };

});
