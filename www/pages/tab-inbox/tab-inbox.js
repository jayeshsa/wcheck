angular.module('starter.controllers')
.controller('InboxCtrl', function($scope,$ionicPlatform, $ionicLoading, $rootScope, $filter, errorPopup, wecheckApi, wecheckStorage) {
$ionicPlatform.on('resume', function(){
      $scope.check_wifi();
});

    $rootScope.newMailCount = 0;
    $scope.messages = [];

    wecheckStorage.getMessages().then(function (res) {
        $scope.messages = res;
        $rootScope.newMailCount += $filter('filter')(res, {is_new: 1}).length;
    });

    $scope.load_data=function(loader){
      
        if(loader){$ionicLoading.show({templateUrl: 'templates/spinner.html'});}
        wecheckApi.getMessages().success(function (res) {
            console.log(res)

            $scope.$broadcast('scroll.refreshComplete');
            $ionicLoading.hide();

            if (!res || res.length == 0) return;

            for (var i = 0; i < res.length; i++) {
                wecheckStorage.insertMessage(res[i]).then(function (item) {
                    $scope.messages = [item].concat($scope.messages);
                }, function (err) {
                    wecheckApi.logError(err);
                    errorPopup.toast(err);
                });
            }
            $rootScope.newMailCount += res.length;

            // init mail count
            var user = wecheckApi.getUser();
            user.mail_count = 0;
            wecheckApi.setUser(user);
            $scope.update_counter();
    },function(err){
         $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
    });
  }



   $scope.doRefresh_inbox=function(flag)
   {
        $scope.check_wifi();
        $scope.load_data(flag);  
   } 

    $scope.doRefresh_inbox(true);

    $scope.update_counter=function()
    {
        
        wecheckStorage.getMessages().then(function (res) {
            $rootScope.newMailCount = $filter('filter')(res, {is_new: 1}).length;
        });
    }

    $scope.updateMessage = function (item) {
        item.isFull = !item.isFull;
        if (item.is_new) {
            item.is_new = false;
            wecheckStorage.markMessageUnread(item);
            $rootScope.newMailCount--;
        }
    };

    //
    $scope.deleteMessage = function (item) {
        wecheckStorage.deleteMessage(item.id).then(function (res) {
            var index = $scope.messages.indexOf(item);
            $scope.messages.splice(index, 1);

            if (item.is_new) {
                $rootScope.newMailCount--;
            }
        }, function (err) {
            console.log(err);
        });
    };
})