angular.module('starter.controllers')
.controller('VenueDirectionCtrl', function($scope,$ionicPlatform, $state, $cordovaGeolocation, uiGmapIsReady, uiGmapGoogleMapApi,errorPopup, wecheckApi) {
  $ionicPlatform.on('resume', function(){
      $scope.check_wifi();
      $scope.check_gps();
      $scope.user = wecheckApi.getUser();
      
});
console.log($scope.user)
console.log($scope.user.profile_image_url)
var profileimgUrl = $scope.user.profile_image_url;
var profileimgUrl2 = profileimgUrl.replace("?type=large", "");
console.log(profileimgUrl2)
// var usr = "<span class='user-input'>You: " + userinput + "</span>";
// var myprofileImage = "<img src=''>"




    $scope.venuePos = wecheckApi.getShareData('venuePos');
    // console.log($scope.venuePos);
    $scope.userPos = {
        latitude: -33.98457440000001,
        longitude: 151.21950000000004
    };

    $scope.map = {
        center: $scope.venuePos,
        zoom: 3,
        control: {},
        options: {
            disableDefaultUI: false,
            zoomControl: false
        }
    };

   
    $scope.userMarkerOption = {
        icon: 'img/user-marker.png'
    };
    

    $scope.options = {
        scrollwheel: false,
        mapTypeControl: false
    };

    $scope.windowOptions = {
        boxClass: "infobox",
        boxStyle: {
            backgroundColor: "#FFFFFF",
            border: "1px solid #800000",
            width: "200px",
            height: "100px"
        },
        disableAutoPan: false,
        maxWidth: 0,
        zIndex: null,
        isHidden: false,
        pane: "floatPane",
        enableEventPropagation: false,
        visible: true
    };





    uiGmapIsReady.promise().then(function (map_instances) {
        $scope.mapRef = $scope.map.control.getGMap();
        return uiGmapGoogleMapApi;
    }).then(function (maps) {
        $scope.directionsDisplay = new maps.DirectionsRenderer({
            suppressMarkers: true,
    polylineOptions: {
      strokeColor: "#800000"
    }
  });

        return $cordovaGeolocation.getCurrentPosition();
    }).then(function (position) {
        $scope.userPos = position.coords;
        console.log($scope.userPos);
        $scope.getDirections($scope.venuePos, $scope.userPos);
    }, function (err) {
        $scope.getDirections($scope.venuePos, $scope.userPos);
    });






    $scope.getDirections = function (dest, origin, type) {
    //   origin={latitude: -30.98457440000001, longitude: 18.477645100000018}; 
        uiGmapGoogleMapApi.then(function (maps) {
            var directionsService = new maps.DirectionsService();
            var originStr = origin.latitude + ", " + origin.longitude;
            var destStr = dest.latitude + ", " + dest.longitude;
            var request = {
                origin: String(originStr),
                destination:String(destStr),
                travelMode: google.maps.TravelMode.DRIVING,
                optimizeWaypoints: true
            };

 
            directionsService.route(request, function (response, status) {
                // console.log(google.maps.DirectionsStatus.OK)
                // console.log(response)
                // console.log(status)
                if (status === google.maps.DirectionsStatus.OK) {
                    console.log("it gone in first stage")
                    $scope.directionsDisplay.setMap(null);
                    $scope.directionsDisplay.setMap($scope.mapRef);

                        // var marker = new google.maps.Marker({
                        // position: {lat: origin.latitude, lng: origin.longitude},
                        // map: $scope.map,
                        // title: '',
                        // icon: profileimgUrl2,
                        // animation:  google.maps.Animation.DROP,
                        // });
                    
                    console.log(origin.latitude)
                    console.log(origin.longitude)
                    var myLatlng = new google.maps.LatLng(origin.latitude, origin.longitude);
                    var marker = new RichMarker({
                    position: myLatlng,
                    map: $scope.map,
                    // content: '<div id="talkbubble">' +
                    //     '<div><img class="buble_img" src="'+profileimgUrl2+'"/></div></div>'
                    content: '<div class="pin" >' +
                        '<div><img class="pin_img" src="'+profileimgUrl2+'"/></div></div>'
                    });





                    // console.log(marker.icon)
// ============================================================================================================================


    


                       
// ============================================================================================================================
                        marker.setMap($scope.mapRef);

                        if($scope.filter_for_restaurant($scope.mydestlocation.category) == "Restaurants"){

                        $scope.markerok = new google.maps.Marker({
                            position: {lat: dest.latitude, lng: dest.longitude},
                            map: $scope.map ,
                            title: '',
                            animation:  google.maps.Animation.DROP,
                            icon: 'img/cutlery_60x60.png'
                            // label:"B",
                        });
                    }else if($scope.filter_for_restaurant($scope.mydestlocation.category) == "Accommodation"){
                        $scope.markerok = new google.maps.Marker({
                            position: {lat: dest.latitude, lng: dest.longitude},
                            map: $scope.map ,
                            title: '',
                            animation:  google.maps.Animation.DROP,
                            icon: 'img/accomo_60x60.png'
                            // label:"B",
                        });
                    }else if($scope.filter_for_restaurant($scope.mydestlocation.category) == "Nightlife"){
                        $scope.markerok = new google.maps.Marker({
                            position: {lat: dest.latitude, lng: dest.longitude},
                            map: $scope.map ,
                            title: '',
                            animation:  google.maps.Animation.DROP,
                            icon: 'img/nightlife_60x60.png'
                            // label:"B",
                        });
                    }
                        new google.maps.event.trigger( $scope.markerok, 'click' );
                        $scope.markerok.setMap($scope.mapRef);

                         $scope.contentStringok = '<div>'+
                                            '<div style="font-size:14px;font-weight:700;color:#800000;margin-left: 65px;line-height: 21px!important;border-bottom: 1px solid #aba9a9 !important;">'+$scope.mydestlocation.title+'</div>'+
                                            '<div class="row" style="padding-0">'+
                                            '<div class="col col-19" style="padding-0"><img style="height: 44px;width: 44px;border-radius: 50%;object-fit: cover;" src='+$scope.mydestlocation.image_url+'>'+'</div>'+
                                            '<div class="col shortdiv"  style="padding-0">'+$scope.myDiscription   + '<a class="read_more1">View</a></div>'+
                                            '<div class="col longdiv"  style="padding-0;display: none">'+$scope.mydestlocation.content   + '<a class="hidemore1">Hide</a></div>'+
                                            '</div>'+
                                            
                                            
                                        '</div>';

                            $scope.infowindowok = new google.maps.InfoWindow({
                                content: $scope.contentStringok
                            });

                            $scope.markerok.addListener('click', function() {
                                $scope.infowindowok.open($scope.map, $scope.markerok);
                            });

                    $scope.directionsDisplay.setDirections(response);

                } else {
                    console.log("it gone in Sec stage")
                   console.log(dest)
                    var flightPlanCoordinates = [
                        new google.maps.LatLng(origin.latitude, origin.longitude),
                        new google.maps.LatLng(dest.latitude, dest.longitude)
                    ];
                    // var flightPath = new google.maps.Polyline({
                    //     path: flightPlanCoordinates,
                    //     strokeColor: "#800000",
                    //     strokeOpacity: 1.0,
                    //     strokeWeight: 3
                    // });

                 
                     var marker = new google.maps.Marker({
                        position: {lat: origin.latitude, lng: origin.longitude},
                        map: $scope.map ,
                        title: '',
                        //  icon: 'img/50x50.png'
                        icon: profileimgUrl2
                    });
                    marker.setMap($scope.mapRef);

                    if($scope.filter_for_restaurant($scope.mydestlocation.category) == "Restaurants"){

                        $scope.marker1 = new google.maps.Marker({
                            position: {lat: dest.latitude, lng: dest.longitude},
                            map: $scope.map ,
                            title: '',
                            icon: 'img/cutlery_60x60.png'
                            // label:"B",
                        });
                    }else if($scope.filter_for_restaurant($scope.mydestlocation.category) == "Accommodation"){
                        $scope.marker1 = new google.maps.Marker({
                            position: {lat: dest.latitude, lng: dest.longitude},
                            map: $scope.map ,
                            title: '',
                            icon: 'img/accomo_60x60.png'
                            // label:"B",
                        });
                    }else if($scope.filter_for_restaurant($scope.mydestlocation.category) == "Nightlife"){
                        $scope.marker1 = new google.maps.Marker({
                            position: {lat: dest.latitude, lng: dest.longitude},
                            map: $scope.map ,
                            title: '',
                            icon: 'img/nightlife_60x60.png'
                            // label:"B",
                        });
                    }

                    //  $scope.marker1 = new google.maps.Marker({
                    //     position: {lat: dest.latitude, lng: dest.longitude},
                    //     map: $scope.map ,
                    //     title: '',
                    //     icon: 'img/blue-map-maker.png'
                    //     // label:"B",
                    // });

                   
                    new google.maps.event.trigger( $scope.marker1, 'click' );
                    $scope.marker1.setMap($scope.mapRef);

                    // var contentString = '<div>'+
                    //                         '<div style="font-size:14px;font-weight:700;color:#800000">'+$scope.mydestlocation.title+'</div>'+
                    //                         '<div style="font-size:12px">'+$scope.mydestlocation.content+'</div>'+
                    //                     '</div>';

                    $scope.contentString = '<div>'+
                                            '<div style="font-size:14px;font-weight:700;color:#800000;margin-left: 65px;line-height: 21px!important;border-bottom: 1px solid #aba9a9 !important;">'+$scope.mydestlocation.title+'</div>'+
                                            '<div class="row" style="padding-0">'+
                                            '<div class="col col-19" style="padding-0"><img style="height: 44px;width: 44px;border-radius: 50%;object-fit: cover;" src='+$scope.mydestlocation.image_url+'>'+'</div>'+
                                            '<div class="col shortdiv"  style="padding-0">'+$scope.myDiscription   + '<a class="read_more1">View</a></div>'+
                                            '<div class="col longdiv"  style="padding-0;display: none">'+$scope.mydestlocation.content   + '<a class="hidemore1">Hide</a></div>'+
                                            '</div>'+
                                            
                                            
                                        '</div>';
                   
                    // var contentString = '<div>'+
                    //                         '<div style="font-size:14px;font-weight:700;color:#800000;margin-left: 65px;">'+$scope.mydestlocation.title+'</div>'+
                    //                         '<div class="row" style="padding-0">'+
                    //                         '<div class="col col-19" style="padding-0"><img style="height: 44px;width: 44px;border-radius: 50%;object-fit: cover;" src='+$scope.mydestlocation.image_url+'>'+'</div>'+
                                            
                    //                         '</div>'+
                    //                         ' <div class="expandable"><div class="col "  style="padding-0">'+$scope.mydestlocation.content+'</div></div>'+
                                           
                    //                     '</div>';
                   

                    $scope.infowindow = new google.maps.InfoWindow({
                        content: $scope.contentString
                    });

                    $scope.marker1.addListener('click', function() {
                        $scope.infowindow.open($scope.map, $scope.marker1);
                    });

                    // $scope.marker1.addListener($scope.infowindow,'closeclick',function(){

                    // console.log("klfjdd")
                    // });

                    errorPopup.showPopup("Alert!!","No Directions avilable for your current location","info")
                    console.log('Directions request failed due to ' + status);
                }
            });


        });
    }


$scope.viewhide = false;

        // $(document).on("click", ".read_more", function() {
        //     // console.log($scope.mydestlocation.content)
        //     // console.log($scope.myDiscription)
        //     $scope.myDiscription = $scope.mydestlocation.content;
        //     $scope.infowindow.close()
        //     $scope.getDirections234();
        //     $scope.infowindow.open($scope.map, $scope.marker1);
        //     $scope.viewhide = true;
        //     $(".read_more").hide();
        // });

$(document).on("click", ".read_more1", function() {
            
            $(".shortdiv").hide();
            $(".longdiv").show();
        });
 $(document).on("click", ".hidemore1", function() {
           
            $(".longdiv").hide();
            $(".shortdiv").show();
        });


    $scope.get_my_location = function(){
   
            var options = {
                enableHighAccuracy: true
            };

        navigator.geolocation.getCurrentPosition(function(pos) {

            var extrajson = [
                {
                    "latitude":pos.coords.latitude,
                    "longitude":pos.coords.longitude
                }
            ];
                
            $scope.map.center = extrajson[0];                 
            }, 
            function(error) {                    
                alert('Unable to get location: ' + error.message);
            }, options);
    };





 
            wecheckApi.getVenues().success(function (res) {
            console.log(res.venues)
                $scope.venues = res.venues;
                for(var i=0;i<$scope.venues.length;i++)
                {
                    // console.log($scope.venues[i])
                    if($scope.venues[i].latitude==$scope.venuePos.latitude && $scope.venues[i].longitude==$scope.venuePos.longitude){
                        $scope.mydestlocation = $scope.venues[i]
                    }
                }
                    // console.log($scope.mydestlocation);
                    $scope.myDiscription = $scope.mydestlocation.content.substring(0,70);
                    // console.log($scope.mydestlocation.category);
                    console.log($scope.filter_for_restaurant($scope.mydestlocation.category));
            });
  
        
        if($scope.mydestlocation == null || $scope.mydestlocation == undefined){

            wecheckApi.getEvents().success(function(res) {
            console.log(res)
                $scope.events = res;
                for(var i=0;i<$scope.events.length;i++)
                    {
                        // console.log($scope.events[i])
                        if($scope.events[i].latitude==$scope.venuePos.latitude && $scope.events[i].longitude==$scope.venuePos.longitude){
                            $scope.mydestlocation = $scope.events[i]
                        }
                        
                    }

                    if($scope.myDiscription != null)
                        {
                            console.log($scope.mydestlocation);
                            $scope.myDiscription = $scope.mydestlocation.content.substring(0,70);
                        }
                    
            });

           }


$scope.filter_for_restaurant = function(str){
        if(str.includes("Restaurants")){return "Restaurants";}
        else if(str.includes("Accommodation")){return "Accommodation";}
        else if(str.includes("Nightlife")){return "Nightlife";}
   };

// =========================== Experiment =============================

$scope.getDirections234 = function () {
   
                    $scope.contentString = '<div>'+
                                            '<div style="font-size:14px;font-weight:700;color:#800000;margin-left: 65px;line-height: 21px!important;border-bottom: 1px solid #aba9a9 !important;">'+$scope.mydestlocation.title+'</div>'+
                                            '<div class="row" style="padding-0">'+
                                            '<div class="col col-19" style="padding-0"><img style="height: 44px;width: 44px;border-radius: 50%;object-fit: cover;" src='+$scope.mydestlocation.image_url+'>'+'</div>'+
                                            '<div class="col"  style="padding-0">'+$scope.myDiscription   +'<a class="read_more">View</a></div>'+
                                            '</div>'+
                                        '</div>';

                    $scope.infowindow = new google.maps.InfoWindow({
                        content: $scope.contentString
                    });

                    
                }













});