angular.module('starter.controllers').controller('reviewCtrl', function($scope,$ionicPlatform, $ionicModal, wecheckApi) {
$ionicPlatform.on('resume', function(){
      $scope.check_wifi();
});

    $scope.venueObj = localStorage.getItem("venueObj");
    $scope.rating = {
        venueId:$scope.venueObj,
        rate: 4,
        title: '',
        content: ''
    };

    $scope.submitReview = function() {
        wecheckApi.reviewVenue($scope.rating).success(function (res) {
            $scope.venue.rated = true;
            $scope.venue.reviews = res;
            $scope.modalCtrl.hide();
        });
    }
})