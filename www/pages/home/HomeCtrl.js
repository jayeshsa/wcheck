app.controller('HomeCtrl', function($scope, $state, $cordovaGeolocation, $ionicLoading, Setting, $rootScope, $cordovaDialogs,
 $filter, wecheckApi, wecheckStorage,$ionicPlatform,errorPopup,$http) {

    var mainUrl = "http://wecheck.co.za/cms/wecheck_oms/";
    var homedata_loadUrl = "homedata_load.php";

   $scope.loading_homedata=true;

$scope.load_trending_data = function(){
    $http.post("http://wecheck.co.za/cms/get_image.php").then(function(res){
            // console.log(res.data.response.catimg);
            $scope.my_trending_images = res.data.response.catimg[0].image_url;
            $scope.my_whatsnew_images = res.data.response.catimg[1].image_url;
          });
};

   $scope.Home_data = [];

    $scope.ratingsObject = {
        iconOn: 'ion-ios-star', //Optional
        iconOff: 'ion-ios-star', //Optional
        iconOnColor: 'rgb(255, 188, 82)', //Optional
        iconOffColor: 'rgb(0, 0, 0)', //Optional
        rating: 4, //Optional
        minRating: 0, //Optional
        readOnly: false, //Optional
        callback: function(rating, index) { //Mandatory    
          $scope.ratingsCallback(rating, index);
        }
      };

      $scope.ratingsCallback = function(rating, index) {
        console.log('Selected rating is : ', rating, ' and index is ', index);
      };
      $scope.gotodetail = function(data, action){
        if (action == "chat") {
            console.log('chat');
            $state.go('app.chat');
            return;
        } else if (action == "email") {
            $state.go('app.sendinquiry');
            console.log('email');
            return;
        } else {
            localStorage.setItem("venueObj", data.id);
            localStorage.setItem("distance", data.distance);
            $state.go('app.venue', {venueData: data});
        }
      }

      $scope.changeaddress = function (data) {
        if (data) {
            var res = data.split(",");
            return res[0]; 
        } else {
            return "";
        }
        
      }
   $scope.load_data=function(loader){
       if(loader){$ionicLoading.show({templateUrl: 'templates/spinner.html'});}
          
        wecheckApi.getVenues().success(function(res) {
            // console.log(res)
             $scope.refresh_inbox();
            $scope.$broadcast('scroll.refreshComplete');
            $scope.loading_homedata=true;


            $scope.homepageSliders = res.homepage_sliders;
            $scope.Home_data = res.venues;
            angular.forEach($scope.Home_data, function(val, key) {
                wecheckApi.getVenueDetail(val.id).success(function (res) {                    
                    $scope.Home_data[key].address = res.address;
                    $scope.Home_data[key].from_time = res.from_time;
                    $scope.Home_data[key].to_time = res.to_time;
                }).error(function () {
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
            })
            console.log($scope.Home_data);
            if (currentPos.latitude) calculateDistance();
            wecheckApi.setShareData('homepageSliders', $scope.homepageSliders);
            $scope.get_location();

        }).error(function () {
          //  $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
            // $scope.loading_homedata=false;
            
        });
   }

    $scope.$on("$ionicView.loaded", function(event, data){
             $ionicPlatform.on('resume', function(){
                $scope.loading_homedata=true;
                $scope.doRefresh_Home(true);
            });
            $scope.doRefresh_Home(true);
    });

    $scope.doRefresh_Home=function(flag)
    {
        $scope.check_wifi();
        $scope.Home_data = []; 
        $scope.load_data(flag);
        // $scope.my_trending_images = null;
        // $scope.my_whatsnew_images = null;
        $scope.load_trending_data();  
    }

 

  


    $scope.data = {};
    $scope.data.sliderOptions = {
        loop: true,
        autoplay: 2000,
        initialSlide: 0,
        direction: 'horizontal', //or vertical
        speed: 500 //0.3s transition
    };

    
    $scope.data.sliderDelegate = null;

    
    $scope.$watch('data.sliderDelegate', function(newVal, oldVal) {
        if (newVal != null) {
            $scope.data.sliderDelegate.on('slideChangeEnd', function() {
                $scope.data.currentPage = $scope.data.sliderDelegate.activeIndex;
                $scope.$apply();
            });
        }
    });


  

    $scope.favoriteVenue = function($event, venue) {
        wecheckApi.favoriteVenue(venue.id, !venue.favorited).success(function (res) {venue.favorited = !venue.favorited;});
        $event.stopPropagation();
        $event.preventDefault();
    };

    $scope.viewVenuInfo = function(venue) {
        // console.log(venue);
        wecheckApi.setShareData('venue', venue);
        $state.go('app.venuInfo');
    };

    var currentPos = {};

    var calculateDistance = function () {
        for (var i = 0; i < $scope.Home_data.length; i++) {
            $scope.Home_data[i].distance =
                wecheckApi.calcDistance(currentPos.latitude, currentPos.longitude, $scope.Home_data[i].latitude, $scope.Home_data[i].longitude);
        }
    }

    $scope.get_location=function()
    {

        $cordovaGeolocation.getCurrentPosition({
                timeout: 10000,
                enableHighAccuracy: false
            }).then(function (position) {
                currentPos = position.coords;
                wecheckApi.setShareData('currentPos', {
                    latitude: currentPos.latitude,
                    longitude: currentPos.longitude
                });

                if ($scope.Home_data.length > 0) calculateDistance();

                // get current address
                var geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(currentPos.latitude, currentPos.longitude);

                geocoder.geocode({ location: latlng }, function(data, status) {
                    if (status == google.maps.GeocoderStatus.OK && data[0]) {
                            wecheckApi.getUser().address = data[0].formatted_address;
                    }
                });
            }, function (err) {
                console.log(err);
            });
    }

   

   

    // get new message count
    wecheckStorage.getMessages().then(function (res) {
        var user = wecheckApi.getUser();
        $rootScope.newMailCount = $filter('filter')(res, {is_new: 1}).length;
        $rootScope.newMailCount += user.mail_count * 1;
console.log("Total message : " + $rootScope.newMailCount)
    });

    // var geocoder = new google.maps.Geocoder();
    // var latlng = new google.maps.LatLng(52.361427, 4.8552631);
    //
    // geocoder.geocode({ location: latlng }, function(data, status) {
    //     console.log(data);
    // });

    // =============================== UMesh ===================================
    // $http.post(mainUrl+homedata_loadUrl).then(function(res){
    //         console.log(res.data);
    //       });
    $scope.viewEventInfo = function(event) {

        wecheckApi.setShareData('event', event);
        $state.go('app.eventInfo');
    };

    var currentPos = wecheckApi.getShareData('currentPos', {});
    var calculateDistanceevent = function () {
        for (var i = 0; i < $scope.events.length; i++) {
            $scope.events[i].distance =
                wecheckApi.calcDistance(currentPos.latitude, currentPos.longitude, $scope.events[i].latitude, $scope.events[i].longitude);
        }
    }
    // scratch data from the server
    $scope.events = [];
    wecheckApi.getEvents().success(function(res) {
        $scope.events = res;
        if (currentPos.latitude) calculateDistanceevent();
    }).error(function () {
        console.log('error');
    });

    $scope.get12hourFormat = function(timeString) {
        if (timeString) {
            var H = timeString.substr(0, 2);
            var h = H % 12 || 12;
            var ampm = (H < 12 || H === 24) ? " AM" : " PM";
            timeString = h + timeString.substr(2, 3) + ampm;
            return timeString;
        } else {
            return "";
        }
    }

});

