'use strict';
(function () {
  angular.module('starter')
  .service('Loginservice', Loginservice);
  Loginservice.$inject = ['$q', '$http', 'serverUrl', 'Setting'];
  function Loginservice ($q, $http, serverUrl, Setting) {
    var self = this;
    self.login = function (userdetails) {
      var q = $q.defer();
      var data1 = {};
      data1.email = userdetails.email;
      data1.password = userdetails.password;

      $http.post(serverUrl + 'login', {'email': userdetails.email, 'password': userdetails.password}).then(function (res) {
        q.resolve(res);
      }, function (error) {
        console.error(error);
        q.reject(error);
      });

      return q.promise;

    };
    self.updateprofile = function (userdetails) {

      var q = $q.defer();
      $http.post(serverUrl + 'updateuser', {"username": Setting.getByType('username'), "address": Setting.getByType('address'), "profile_pic": userdetails, "id": Setting.getByType('userid')}).then(function (res) {
        console.info(res);
        q.resolve(res);
      }, function (error) {
        console.error(error);
        q.reject(error);
      });

      return q.promise;
    };

    return self;
  }
})();
