'use strict';
(function() {
    angular.module('starter')
        .service('Facebook', Facebook);
    Facebook.$inject = ['$http', '$q', '$ionicPlatform', '$cordovaFacebook', 'serverUrl'];

    function Facebook($http, $q, $ionicPlatform, $cordovaFacebook, serverUrl) {
        var self = this;
        self.login = function() {
            console.log('method clicked');
            var q = $q.defer();
            $cordovaFacebook.getLoginStatus().then(
                function(loginStatus) {
                    if (loginStatus.status === 'connected') {
                        self.getProfileAndLogin(loginStatus).then(function(resp) {
                            q.resolve(resp);
                        }, function(error) {
                            q.reject(error);
                        });
                    } else {
                        $cordovaFacebook.login(['public_profile', 'email']).then(function(resp) {
                            self.getProfileAndLogin(resp).then(function(resp) {
                                q.resolve(resp);
                            }, function(error) {
                                q.reject(error);
                            });
                        }, function(error) {
                            q.reject(error.errorMessage);
                        });
                    }
                },
                function(loginStatusError) {
                    console.error(loginStatusError);
                    q.reject(loginStatusError);
                });
            return q.promise;
        };

        self.getProfileAndLogin = function(resp) {
            var q = $q.defer();
            window.facebookConnectPlugin.api('/me?fields=first_name,last_name,email,picture.width(1000).height(1000),name&access_token=' + resp.accessToken, null,
                function(response) {
                    var param = {
                        provider: 'facebook',
                        facebook: {
                            uid: response.id,
                            provider: 'facebook',
                            first_name: response.name,
                            last_name: '',
                            email: response.email,
                            image: 'http://graph.facebook.com/' + response.id + '/picture?type=large'
                        }
                    };
                    console.info(response);
                    $http.post(serverUrl + 'social_login', {
                        "email": response.email,
                        "username": response.name,
                        "profile_pic": response.picture.data.url,
                        "regtype": 1
                    }).then(function(res) {
                        console.info(res);
                        q.resolve(res);
                    }, function(error) {
                        console.error(error);
                        q.reject(error);
                    });
                },
                function(response) {
                    q.reject(response);
                });
            return q.promise;
        };
        return self;
    }
})();