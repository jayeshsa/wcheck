angular.module('starter')
.config(function($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider, $ionicConfigProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyCgoGnlh6BVGUrIT4mrQUVDZnY7fvgMPE0',
        v: '3.24',
        libraries: 'weather,geometry,visualization'
    });
    $ionicConfigProvider.views.maxCache(0);
    $stateProvider
    .state('default', {
        url: '/default',
        abstract: true,
        templateUrl: 'pages/default/default.html',
        controller: 'AppCtrl'
    })
    .state('default.login', {
        url: '/login',
        views: {
            'defaultView': {
                templateUrl: 'pages/login/login.html',
                controller: 'LoginCtrl'
            }
        }
    })
    .state('default.signup', {
        url: '/signup',
        views: {
            'defaultView': {
                templateUrl: 'pages/signup/signup.html',
                controller: 'signupCtrl'
            }
        }
    })
    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'pages/menu/menu.html',
        controller: 'AppCtrl'

    })
    .state('app.home', {
        url: '/home',
        views: {
            'menuContent': {
                templateUrl: 'pages/home/home.html',
                controller: 'HomeCtrl'
            }
        }
    })
    .state('app.venue', {
        url: '/venue',
        params: {
            venueData: null
        },
        views: {
            'menuContent': {
                templateUrl: 'pages/venue/venue.html',
                controller: 'VenueCtrl'
            }
        }
    })
    .state('app.watsnew', {
        url: '/watsnew',
        views: {
            'menuContent': {
                templateUrl: 'pages/watsnew/watsnew.html',
                controller: 'watsnewCtrl'
            }
        }
    })
    .state('app.Restaurants', {
        url: '/Restaurants',
        views: {
            'menuContent': {
                templateUrl: 'pages/Restaurants/Restaurants.html',
                controller: 'RestaurantsCtrl'
            }
        }
    })
    .state('app.Nightlife', {
        url: '/Nightlife',
        views: {
            'menuContent': {
                templateUrl: 'pages/Nightlife/Nightlife.html',
                controller: 'NightlifeCtrl'
            }
        }
    })
    .state('app.Checklist', {
        url: '/Checklist',
        views: {
            'menuContent': {
                templateUrl: 'pages/Checklist/Checklist.html',
                controller: 'ChecklistCtrl'
            }
        }
    })
    .state('app.Accommodation', {
        url: '/Accommodation',
        views: {
            'menuContent': {
                templateUrl: 'pages/Accommodation/Accommodation.html',
                controller: 'AccommodationCtrl'
            }
        }
    })
    .state('app.tranding', {
        url: '/tranding',
        views: {
            'menuContent': {
                templateUrl: 'pages/tranding/tranding.html',
                controller: 'trandingCtrl'
            }
        }
    })
    .state('app.sub_filter', {
        url: '/sub_filter',
        views: {
            'menuContent': {
                templateUrl: 'pages/sub_filter/sub_filter.html',
                controller: 'sub_filterCtrl'
            }
        }
    })
    .state('app.help', {
        url: '/help',
        views: {
            'menuContent': {
                templateUrl: 'pages/help/help.html',
                controller: 'HelpCtrl'
            }
        }
    })
    .state('app.settings', {
        url: '/settings',
        views: {
            'menuContent': {
                templateUrl: 'pages/settings/settings.html',
                controller: 'SettingsCtrl'
            }
        }
    })
    .state('app.profile', {
        url: '/profile',
        views: {
            'menuContent': {
                templateUrl: 'pages/profile/profile.html',
                controller: 'ProfileCtrl'
            }
        }
    })
    .state('app.map', {
        url: '/map',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'pages/tab-map/tab-map.html',
                controller: 'MapCtrl'
            }
        }
    })
    .state('app.inbox', {
        url: '/inbox',
        views: {
            'menuContent': {
                templateUrl: 'pages/tab-inbox/tab-inbox.html',
                controller: 'InboxCtrl'
            }
        }
    })
    .state('app.venuInfo', {
        url: '/venuInfo',
        views: {
            'menuContent': {
                templateUrl: 'pages/venu_info/venu_info.html',
                controller: 'VenuCtrl'
            }
        }
    })
    .state('app.scanQR', {
        url: '/scanQR',
        views: {
            'menuContent': {
                // templateUrl: 'pages/scan-qr/scanQR.html',
                controller: 'ScanQRCtrl'
            }
        }
    })
    .state('app.venuMenu', {
        url: '/venuMenu',
        views: {
            'menuContent': {
                templateUrl: 'pages/venu_menu/venu_menu.html',
                controller: 'VenuMenuCtrl'
            }
        }
    })
    .state('app.venuGallery', {
        url: '/venuGallery',
        views: {
            'menuContent': {
                templateUrl: 'pages/venu_gallery/venu_gallery.html',
                controller: 'VenuGalleryCtrl'
            }
        }
    })
    .state('app.venuSocial', {
        url: '/venuSocial',
        views: {
            'menuContent': {
                templateUrl: 'pages/venu_social/venu_social.html',
                controller: 'VenuSocialCtrl'
            }
        }
    })
    .state('app.venuVideo', {
        url: '/venuVideo',
        views: {
            'menuContent': {
                templateUrl: 'pages/venu_video/venu_video.html',
                controller: 'VenuVideoCtrl'
            }
        }
    })
    .state('app.venueDirection', {
        url: '/venueDirection',
        views: {
            'menuContent': {
                templateUrl: 'pages/venue_direction/venue_direction.html',
                controller: 'VenueDirectionCtrl'
            }
        }
    })
    .state('app.eventList', {
        url: '/eventList',
        views: {
            'menuContent': {
                templateUrl: 'pages/event_list/event_list.html',
                controller: 'EventListCtrl'
            }
        }
    })
    .state('app.eventInfo', {
        url: '/eventInfo',
        views: {
            'menuContent': {
                templateUrl: 'pages/event_info/event_info.html',
                controller: 'EventInfoCtrl'
            }
        }
    })
    .state('app.myRewards', {
        url: '/my_rewards',
        views: {
            'menuContent': {
                templateUrl: 'pages/my_rewards/my_rewards.html',
                controller: 'RewardsCtrl'
            }
        }
    })
    .state('forgotpassword', {
        url: '/forgotpassword',
        cache: false,
        controller: 'forgotPasswordCtrl',
        templateUrl: 'pages/forgotpassword/forgotpassword.html'
    })
    .state('app.search', {
        url: '/search',
        views: {
            'menuContent': {
                templateUrl: 'pages/search/search.html',
                controller: 'SearchCtrl'
            }
        }
    })
    .state('app.filter', {
        url: '/filter',
        views: {
            'menuContent': {
                templateUrl: 'pages/filter/filter.html',
                controller: 'filterCtrl'
            }
        }
    })
    .state('app.chat', {
        url: '/chat',
        views: {
            'menuContent': {
                templateUrl: 'pages/chat/chat.html',
                // controller: 'chatCtrl'
            }
        }
    })
    .state('app.sendinquiry', {
        url: '/sendinquiry',
        views: {
            'menuContent': {
                templateUrl: 'pages/send_inquiry/send_inquiry.html',
                controller: 'sendInquiryCtrl'
            }
        }
    })
    // $urlRouterProvider.otherwise('/app/home');
});