(function () {
  'use strict';
  angular.module('starter').factory('Validation', [Validation]);
  function Validation () {
    var self = this;
    self.isValid = function(data){
      if (data == undefined || data == null || data =="") {
        return false;
      }else{
        return true;
      };
    };
    self.isValidLength = function(field, length){
      var data = field.toString().length
      if (data > length) {
        return false;
      }else{
        return true;
      };
    };
    return self;
  }
})();
