'use strict';
angular.module('wecheck.setting', []).service('Setting', function () {
    var self = this;

    self.init = function() {
        if (window.localStorage['UserProfile'] === undefined) {
            var config = {
                first_name: '',
                last_name: ''
            };
            window.localStorage['UserProfile'] = JSON.stringify(config);
        }
        return self;
    };

    self.getByType = function(type) {
        if (window.localStorage['UserProfile']) {
            return JSON.parse(window.localStorage['UserProfile'])[type]
        }
    }
    self.setByType = function(type, value) {
        if (window.localStorage['UserProfile'] === undefined) {
            var config = {
                first_name: '',
                last_name: ''
            };
            window.localStorage['UserProfile'] = JSON.stringify(config);
        }
        var config = JSON.parse(window.localStorage['UserProfile']);
        config[type] = value;
        window.localStorage['UserProfile'] = JSON.stringify(config);
        return value;
    };
    self.currentUser = function() {
        return JSON.parse(window.localStorage['UserProfile'])
    }
    self.resetUserProfile = function() {
        var config = {
            first_name: '',
            last_name: ''
        };
        window.localStorage['UserProfile'] = JSON.stringify(config);
    }

    this.setData = function (key, value) {
      window.localStorage['wecheck_' + key] = JSON.stringify(value);
    }

    this.getData = function (key, initValue) {
      return window.localStorage['wecheck_' + key] ?
              JSON.parse(window.localStorage['wecheck_' + key]) : (initValue ? initValue : false);
    }

    this.clear = function () {
        window.localStorage.clear();
    }

    return self;
});
