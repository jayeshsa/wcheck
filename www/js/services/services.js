angular.module('starter.services', [])

.service('PopupService', function ($q, $ionicPopup) {
  this.showAlert = function(data) {
    var deferred = $q.defer();
    var alertPopup = $ionicPopup.alert(data);
    deferred.resolve(alertPopup);

    return deferred.promise;
 };
})

.service('StoreRewardsOfflineService', [function(){
	this.getRewards = function(){
		return JSON.parse(localStorage.getItem("rewards", "[]"));
	}

	this.updateRewards = function(rewards){
		localStorage.setItem("rewards", JSON.stringify(rewards));
	}
}]);