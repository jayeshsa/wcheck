'use strict'
angular.module('wecheck')
.service('wecheckStorage', function ($http, $q, $cordovaSQLite, wecheckApi) {
    var db;

    this.openDB = function () {
        if (window.cordova) {
            db = $cordovaSQLite.openDB({name: "wecheck.db", location: 0}); //device
        } else {
            db = window.openDatabase("wecheck.db", '1', 'my', 1024 * 1024 * 100); // browser
        }
        $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS message (id integer primary key, user_id integer, title text, image_url text, content text, date text, is_new integer)");
    }

    // $cordovaSQLite.execute(db, "DROP TABLE message");

    this.getMessages = function () {
        var user = wecheckApi.getUser();
        var d = $q.defer();
        var query = "SELECT * FROM message WHERE user_id=?";

        document.addEventListener("deviceready", function() {
            $cordovaSQLite.execute(db, query, [user.id]).then(function(res) {
                var result = [];
                if (res && res.rows) {
                    for (var i = 0; i < res.rows.length; i++) {
                        result.push(res.rows.item(i));
                    }
                }
                d.resolve(result);
            }, function (err) {
                d.reject(err);
            });
        }, false);

        return d.promise;
    }

    this.insertMessage = function (info) {
        var user = wecheckApi.getUser();
        var d = $q.defer();
        var query = "INSERT INTO message (user_id, title, image_url, content, date, is_new) VALUES (?,?,?,?,?,?)";

        document.addEventListener("deviceready", function() {
            $cordovaSQLite.execute(db, query, [user.id, info.title, info.image_url, info.content, info.date, 1]).then(function (res) {
                info.id = res.insertId;
                info.is_new = 1;
                d.resolve(info);
            }, function (err) {
                d.reject(err);
            });
        }, false);

        return d.promise;
    };

    this.markMessageUnread = function (info) {
        var d = $q.defer();
        var query = "UPDATE message SET is_new = 0 WHERE id=?";

        document.addEventListener("deviceready", function() {
            $cordovaSQLite.execute(db, query, [info.id]).then(function (res) {
                d.resolve(true);
            }, function (err) {
                console.log(err);
                d.reject(err);
            });
        }, false);

        return d.promise;
    };

    this.deleteMessage = function (messageId) {
        var d = $q.defer();
        var query = "DELETE FROM message WHERE id=(?)";

        document.addEventListener("deviceready", function() {
            $cordovaSQLite.execute(db, query, [messageId]).then(function (res) {
                d.resolve(true);
            }, function (err) {
                console.log(err);
                d.reject(err);
            });
        }, false);

        return d.promise;
    };
});