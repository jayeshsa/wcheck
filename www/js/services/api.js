'use strict';

angular.module('wecheck', ['wecheck.setting'])
.service('wecheckApi', function ($http, $q, Setting) {
   // var apiURL = 'http://wedwise.co.za/cms/wp-json/wecheckapi/v1';
    // var apiURL = 'http://wedwise.co.za/cms/wp-json/wecheckapi/v1';
    //var apiURL = 'http://wedwise.co.za/cms/wp-json/wecheckapi/v1';
    // var apiURL = 'http://wedwise.co.za/cms/index.php/wp-json/wecheckapi/v1';
    var apiURL = 'http://wedwise.co.za/cmsmainnew/index.php/wp-json/wecheckapi/v1';
    var apiKey = Setting.getData('api_key');
    var sharedStorage = [];
    var currentUser = false;

    function deg2rad(deg) {
        return deg * (Math.PI/180);
    }

    this.calcDistance = function(lat1,lon1,lat2,lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2-lat1);  // deg2rad below
        var dLon = deg2rad(lon2-lon1);
        var a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c; // Distance in km
        return d;
    }

    this.setApiKey = function (key) {
        apiKey = key;
    }

    this.login = function (info) {
        var q = $q.defer();

        $http.post(apiURL + '/login', {'email': info.email, 'password': info.password}).then(function (res) {
            apiKey = res.data.api_key;
            Setting.setData('api_key', apiKey);
            q.resolve(res.data);
        }, function (error) {
            q.reject(error);
        });

        return q.promise;
    }

    this.socialLogin = function (info) {
        var q = $q.defer();

        $http.post(apiURL + '/social-login', info).then(function (res) {
            apiKey = res.data.api_key;
            Setting.setData('api_key', apiKey);
            q.resolve(res.data);
        }, function (error) {
            q.reject(error);
        });

        return q.promise;
    }

    this.signUp = function (info) {
        return $http.post(apiURL + '/signup', info);
    }

    this.resetPassword = function (email) {
        return $http.post(apiURL + '/reset-password', {email: email});
    }


    this.getShareData = function (key, initValue) {
        return Setting.getData(key, initValue);
    }

    this.setShareData = function (key, data) {
        Setting.setData(key, data);
    }


    this.getVenues = function () {
        return $http.get(apiURL + '/venues?api_key=' + apiKey);
    }

    this.getRestaurants = function () {
        return $http.get(apiURL + '/vendors');
    }

    this.getVenueDetail = function (id) {
        return $http.get(apiURL + '/venues/detail?api_key=' + apiKey + '&id=' + id);
    }

    this.favoriteVenue = function (venueId, state) {
        return $http.post(apiURL + '/venues/favorite?api_key=' + apiKey, {id: venueId, state: state});
    }

    this.subscribeVenue = function (venueId, state) {
        return $http.post(apiURL + '/venues/subscribe?api_key=' + apiKey, {id: venueId, state: state});
    }


    this.reviewVenue = function (info) {
        return $http.post(apiURL + '/venues/review?api_key=' + apiKey, info);
    }

    this.getReviews = function (venueId) {
        return $http.get(apiURL + '/reviews?api_key=' + apiKey + '&venueId=' + venueId);
    }

    this.getMessages = function () {
        return $http.get(apiURL + '/messages?api_key=' + apiKey);
    }

    this.getMaps = function () {
        return $http.get(apiURL + '/maps?api_key=' + apiKey);
    }

    this.uploadUserGallery = function (venueId, data) {
        return $http({
            url: apiURL + '/gallery/upload?api_key=' + apiKey,
            dataType: 'json',
            method: 'POST',
            data: {venueId: venueId, picData: data},
            headers: {
                "Content-Type": "application/json"
            }
        });
    }

    this.getEvents = function () {
        return $http.get(apiURL + '/events?api_key=' + apiKey);
    }

    this.getEventDetail = function (id) {
        return $http.get(apiURL + '/events/detail?api_key=' + apiKey + '&id=' + id);
    }

    this.favoriteEvent = function (eventId, state) {
        return $http.post(apiURL + '/events/favorite?api_key=' + apiKey, {id: eventId, state: state});
    }

    this.getsearch = function (stext) {
        return $http.post(apiURL + '/search', {text: stext});
    }

    this.subscribeEvent = function (eventId, state) {
        return $http.post(apiURL + '/events/subscribe?api_key=' + apiKey, {id: eventId, state: state});
    }

    this.getInstagram = function (venueId) {
        return $http.get(apiURL + '/instagram?venueId=' + venueId + '&api_key=' + apiKey);
    }

    this.getUserInfo = function () {
        return $http.get(apiURL + '/user/info?api_key=' + apiKey);
    }

    this.updateProfileImage = function (data) {
        return $http.post(apiURL + '/user/update-profile?api_key=' + apiKey, data);
    }

    this.getcategory = function () {
        return $http.get(apiURL + '/categories');
    }
    this.getUser = function () {
        if (!currentUser) {
            currentUser = Setting.getData('user');
        }
        if (!currentUser) {
            currentUser = {};
        }
        return currentUser;
    }

    this.setUser = function (user) {
        currentUser = user;
        Setting.setData('user', user);

        return currentUser;
    }

    this.logError = function (err) {
        return $http.post(apiURL + '/log', {content: err});
    };

});
