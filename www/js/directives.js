var app = angular.module('starter.directives', []);

app.directive('setFullHeight', function($window){
	return{
		link: function(scope, element, attrs){
			element.css('height', $window.innerHeight - 43 + 'px');
		}
	}
});

app.directive('fullHeight', function($window){
	return{
		link: function(scope, element, attrs){
			var mapContainer = element[0].querySelector('.angular-google-map-container');
			angular.element(mapContainer).css('height', $window.innerHeight - (44 + 49) + 'px');
		}
	}
});
app.directive('onErrorSrc', function() {
    return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.onErrorSrc) {
              attrs.$set('src', attrs.onErrorSrc);
            }
          });
        }
    }
});
app.directive('searchInput', function() {    
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
        	var parent = angular.element(element).parent();
        	parent = angular.element(parent).parent();
        	parent = angular.element(parent).parent();
            element.bind('focusin', function() {
            	console.log(parent);
            	angular.element(parent).addClass('active-search');
            });

            element.bind('focusout', function() {
            	console.log(parent);
                angular.element(parent).removeClass('active-search');
            });
        }
    };        
});

app.directive('popupSelect', function($timeout, $q, $ionicPopup) {
    return {
        restrict: 'EA',
        scope: {
            options: '=',
            title: '@',
            valueField: '@',
            displayField: '@'
        },
        require:'?ngModel',
        link: function($scope, element, attrs, ngModel) {
            $scope.selected = {};
            $scope.display = {};
            var title = $scope.title || 'Select option';
            var displayElement = angular.element(element.find('span'));
            displayElement.html(title);
            element.on('click', function(){
                console.log(JSON.stringify($scope.options));
                // scope.choice = '';
                var myPopup = $ionicPopup.show({
                    templateUrl: 'templates/select-popup.html',
                    title: title,
                    cssClass: 'popup-select',
                    scope: $scope,
                    buttons: [
                        {
                            text: 'Close'
                        },
                        {
                            text: '<b>Select</b>',
                            type: 'button-custom',
                            onTap: function(e) {
                                e.preventDefault();
                                ngModel.$setViewValue($scope.selected.value);
                                ngModel.$render();
                                // $scope.model = $scope.selected.value;
                                if ($scope.display.value) {
                                    displayElement.html($scope.display.value);
                                } else {
                                    displayElement.html(title);
                                }
                                myPopup.close();
                            }
                        }
                    ]
                });

                myPopup.then(function(res) {
                    console.log('Tapped!', res);
                });
            });
        }
    };
});







app.factory('ClosePopupService', function($document, $ionicPopup, $timeout){
  var lastPopup;
  return {
    register: function(popup) {
      $timeout(function(){
        var element = $ionicPopup._popupStack.length>0 ? $ionicPopup._popupStack[0].element : null;
        if(!element || !popup || !popup.close) return;
        element = element && element.children ? angular.element(element.children()[0]) : null;
        lastPopup  = popup;
        var insideClickHandler = function(event){
          event.stopPropagation();
        };
        var outsideHandler = function() {
          popup.close();
        };
        element.on('click', insideClickHandler);
        $document.on('click', outsideHandler);
        popup.then(function(){
          lastPopup = null;
          element.off('click', insideClickHandler);
          $document.off('click', outsideHandler);
        });
      });
    },
    closeActivePopup: function(){
      if(lastPopup) {
        $timeout(lastPopup.close);
        return lastPopup;
      }
    }
  };
})



app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.directive("limitTo", [function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on("keypress", function(e) {
                if (this.value.length == limit) e.preventDefault();
            });
        }
    }
}]);

app.factory('Base64', function(){
    var self = this;
    self.encode = function (input) {
        // Converts each character in the input to its Unicode number, then writes
        // out the Unicode numbers in binary, one after another, into a string.
        // This string is then split up at every 6th character, these substrings
        // are then converted back into binary integers and are used to subscript
        // the "swaps" array.
        // Since this would create HUGE strings of 1s and 0s, the distinct steps
        // above are actually interleaved in the code below (ie. the long binary
        // string, called "input_binary", gets processed while it is still being
        // created, so that it never gets too big (in fact, it stays under 13
        // characters long no matter what).

        // The indices of this array provide the map from numbers to base 64
        var swaps = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9","+","/"];
        var input_binary = "";      // The input string, converted to Unicode numbers and written out in binary
        var output = "";        // The base 64 output
        var temp_binary;        // Used to ensure the binary numbers have 8 bits
        var index;      // Loop variable, for looping through input
        for (index=0; index < input.length; index++){
            // Turn the next character of input into astring of 8-bit binary
            temp_binary = input.charCodeAt(index).toString(2);
            while (temp_binary.length < 8){
                temp_binary = "0"+temp_binary;
            }
            // Stick this string on the end of the previous 8-bit binary strings to
            // get one big concatenated binary representation
            input_binary = input_binary + temp_binary;
            // Remove all 6-bit sequences from the start of the concatenated binary
            // string, convert them to a base 64 character and append to output.
            // Doing this here prevents input_binary from getting massive
            while (input_binary.length >= 6){
                output = output + swaps[parseInt(input_binary.substring(0,6),2)];
                input_binary = input_binary.substring(6);
            }
        }
        // Handle any necessary padding
        if (input_binary.length == 4){
            temp_binary = input_binary + "00";
            output = output + swaps[parseInt(temp_binary,2)];
            output = output + "=";
        }
        if (input_binary.length == 2){
            temp_binary = input_binary + "0000";
            output = output + swaps[parseInt(temp_binary,2)];
            output = output + "==";
        }
        // Output now contains the input in base 64
        return output;
    };

    self.decode = function (input) {
        // Takes a base 64 encoded string "input", strips any "=" or "==" padding
        // off it and converts its base 64 numerals into regular integers (using a
        // string as a lookup table). These are then written out as 6-bit binary
        // numbers and concatenated together. The result is split into 8-bit
        // sequences and these are converted to string characters, which are
        // concatenated and output.
        input = input.replace("=","");      // Padding characters are redundant
        // The index/character relationship in the following string acts as a
        // lookup table to convert from base 64 numerals to Javascript integers
        var swaps = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        var output_binary = "";
        var output = "";
        var temp_bin = "";
        var index;
        for (index=0; index < input.length; index++) {
            temp_bin = swaps.indexOf(input.charAt(index)).toString(2);
            while (temp_bin.length < 6) {
                // Add significant zeroes
                temp_bin = "0"+temp_bin;
            }
            while (temp_bin.length > 6) {
                // Remove significant bits
                temp_bin = temp_bin.substring(1);
            }
            output_binary = output_binary + temp_bin;
            while (output_binary.length >= 8) {
                output = output + String.fromCharCode(parseInt(output_binary.substring(0,8),2));
                output_binary = output_binary.substring(8);
            }
        }
        return output;
    };
    
    return self;
})

app.factory('TwitterREST', function($http, $q, Base64){

    var self = this;
    var authorization = null;
        var consumerKey = "qzRXgkI7enflNJH1lWFvujT2P";
        var consumerSecret = "8e7E7gHuTwyDHw9lGQFO73FcUwz9YozT37lEvZulMq8FXaPl8O";
    var twitterTokenURL = "https://api.twitter.com/oauth2/token";
    var twitterStreamURL = "https://api.twitter.com/1.1/search/tweets.json?q="; //url query, this one is for hash tags
    var qValue = "%23belgrade"; //hash tag %23 is for #
    var numberOfTweets = "&count=10";

    self.sync = function () {
        var def = $q.defer();
        //get authorization token
        self.getAuthorization().then(function(){
            var req1 = {
                method: 'GET',
                url: twitterStreamURL+qValue+numberOfTweets,
                headers: {
                    'Authorization': 'Bearer '+authorization.access_token,
                    'Content-Type': 'application/json'
                },
                cache:true
            };
            // make request with the token
            $http(req1).
                success(function(data, status, headers, config) {
                    def.resolve(data);
                }).
                error(function(data, status, headers, config) {

                    def.resolve(false);
                });
        });
        return def.promise;
    };

    self.getAuthorization = function () {
      var def = $q.defer();
      var base64Encoded;

      var combined = encodeURIComponent(consumerKey) + ":" + encodeURIComponent(consumerSecret);

      base64Encoded = Base64.encode(combined);

      // Get the token
      $http.post(twitterTokenURL,"grant_type=client_credentials", {headers: {'Authorization': 'Basic ' + base64Encoded, 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}}).
          success(function(data, status, headers, config) {
            authorization = data;
            if (data && data.token_type && data.token_type === "bearer") {
                def.resolve(true);
            }
          }).
          error(function(data, status, headers, config) {
            def.resolve(false);
          });
      return def.promise;
    };

    return self;
});

app.filter('formatTwitterDate', function() {
    /*
        Calculates time ago
    */
    return function(input) {
            var rightNow = new Date();
            var then = new Date(input);

            var diff = rightNow - then;
            var second = 1000,
                minute = second * 60,
                hour = minute * 60,
                day = hour * 24,
                week = day * 7;
            if (isNaN(diff) || diff < 0) {
                return ""; // return blank string if unknown
            }
            if (diff < second * 2) {
                // within 2 seconds
                return "right now";
            }
            if (diff < minute) {
                return Math.floor(diff / second) + " seconds ago";
            }
            if (diff < minute * 2) {
                return "about 1 minute ago";
            }
            if (diff < hour) {
                return Math.floor(diff / minute) + " minutes ago";
            }
            if (diff < hour * 2) {
                return "about 1 hour ago";
            }
            if (diff < day) {
                return  Math.floor(diff / hour) + " hours ago";
            }
            if (diff > day && diff < day * 2) {
                return "yesterday";
            }
            if (diff < day * 365) {
                return Math.floor(diff / day) + " days ago";
            }
            else {
                return "over a year ago";
            }
    };
})

app.filter('formatTwitterText', function() {
    /*
        Parses tweet text for links, hashes etc.
    */
    return function(tweet) {
        TweetFunc = {
            link: function (tweet) {
                return tweet.replace(/\b(((https*\:\/\/)|www\.)[^\"\']+?)(([!?,.\)]+)?(\s|$))/g, function (link, m1, m2, m3, m4) {
                    var http = m2.match(/w/) ? 'http://' : '';
                    return '<a class="twtr-hyperlink" ng-click="innapBrowser(\'' + http + m1 + '\')">' + ((m1.length > 25) ? m1.substr(0, 24) + '...' : m1) + '</a>' + m4;
                });
            },
            at: function (tweet) {
                return tweet.replace(/\B[@＠]([a-zA-Z0-9_]{1,20})/g, function (m, username) {
                    return '<a class="twtr-atreply" ng-click="innapBrowser(\'http://twitter.com/intent/user?screen_name=' + username + '\')">@' + username + '</a>';
                });
            },
            list: function (tweet) {
                return tweet.replace(/\B[@＠]([a-zA-Z0-9_]{1,20}\/\w+)/g, function (m, userlist) {
                    return '<a class="twtr-atreply" ng-click="innapBrowser(\'http://twitter.com/' + userlist + '\')">@' + userlist + '</a>';
                });
            },
            hash: function (tweet) {
                return tweet.replace(/(^|\s+)#(\w+)/gi, function (m, before, hash) {
                    return before + '<a target="_blank" class="twtr-hashtag" ng-click="innapBrowser(\'http://twitter.com/search?q=%23' + hash + '\')">#' + hash + '</a>';
                });
            }
        }

        return TweetFunc.hash(TweetFunc.at(TweetFunc.list(TweetFunc.link(tweet))));
    }
});
// app.filter("notEmpty", function(){ 
//   return function(input){
//     var output=[];
//     for(var i=0;i<input.length;i++){
//       if(input[i]){
//         output.push(input[i]);
//       }
//     }
//     return output;
// }});
