'use strict'
angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $rootScope,$timeout, $state, wecheckApi, Setting,wecheckStorage) {
	$rootScope.searchModal = null;
	$scope.reviewModal = {};
	$scope.user = wecheckApi.getUser();

$scope.refresh_inbox=function()
{
	 wecheckApi.getMessages().success(function (res) {
		
        if (!res || res.length == 0) return;

        for (var i = 0; i < res.length; i++) {
            wecheckStorage.insertMessage(res[i]).then(function (item) {
                $scope.messages = [item].concat($scope.messages);
            }, function (err) {
                wecheckApi.logError(err);
                errorPopup.toast(err);
            });
        }
        $rootScope.newMailCount += res.length;

        // init mail count
        var user = wecheckApi.getUser();
        user.mail_count = 0;
        wecheckApi.setUser(user);
    });
}
	

	$ionicModal.fromTemplateUrl('templates/review-modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.reviewModal = modal;
	});

	$scope.openSearchModal = function() {
		$state.go("app.search");
		// $ionicModal.fromTemplateUrl('templates/search-modal.html', {
		// 	scope: $scope,
		// 	animation: 'slide-in-up'
		// }).then(function(modal) {
		// 	$rootScope.searchModal = modal;
		// 	$rootScope.searchModal.show();
		// });
		
	};

	$scope.closeSearchModal = function() {
		$rootScope.searchModal.hide();
	};

	$scope.preventEvent = function($event) {
		$event.stopPropagation();
		$event.preventDefault();
	};
	$scope.signout = function() {
		window.localStorage.clear();
		$state.go('default.login');
	};

	Array.prototype.contains = function(text) {
		return this.indexOf(text) !== -1;
	};
	$scope.myprofileimage = window.localStorage['profilepic'];
	$scope.myname = Setting.getByType('username');

	$scope.$on('addressrecived', function(event, data) {
		$scope.address = data;
	});
	$scope.$on('profilepicupdated', function(event, data) {
		console.info(data);
		$scope.myprofileimage = data;
	});
})





