'use strict';
(function () {
  angular.module('starter')
  .service('CameraServ', CameraServ);
  CameraServ.$inject = ['$q', '$cordovaCamera', '$ionicActionSheet'];
  function CameraServ ($q, $cordovaCamera, $ionicActionSheet) {
    var self = this;
    self.getPic = function (options) {
      var q = $q.defer();
      $cordovaCamera.getPicture(options).then(function (imageURI) {
        q.resolve(imageURI);
      }, function (err) {
        q.reject(err);
      });
      return q.promise;
    };

    self.selectAndGetPic = function () {
      var q = $q.defer();
      //var selected;
      $ionicActionSheet.show({
        buttons: [
          { text: 'Camera' },
          { text: 'Gallery' },
          { text: 'Cancel' }
        ],
        cancel: function () {
          // add cancel code..
        },
        buttonClicked: function (index) {
          if (index === 0) {
            self.takePicture('camera').then(function (resp) {
              q.resolve(resp);
            }, function (err) {
              q.reject(err);
            });
          } else if (index === 1) {
            self.takePicture('library').then(function (resp) {
              q.resolve(resp);
            }, function (err) {
              q.reject(err);
            });
          }
          return true;
        }
      });
      return q.promise;
    };

    self.takePicture = function (type) {
      var q = $q.defer();
      var source;
      if (type === 'camera') {
        source = navigator.camera.PictureSourceType.CAMERA;
      } else {
        source = navigator.camera.PictureSourceType.PHOTOLIBRARY;
      }
      var options = {
        quality: 75,
        // destinationType : Camera.DestinationType.FILE_URI,
        destinationType: navigator.camera.DestinationType.DATA_URL,
        sourceType: source,
        // allowEdit : true,
        encodingType: Camera.EncodingType.JPEG,
        correctOrientation: true,
        targetWidth: 300,
        targetHeight: 300,
        // popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };

      self.getPic(options).then(function (resp) {
        q.resolve(resp);
      }, function (err) {
        if (err === 'Camera cancelled') {
          q.reject('Camera cancelled');
        } else {
          q.reject(err);
        }
      }, function () {
      });
      return q.promise;
    };
    self.takePicturewithoutbase64 = function (type) {
      var q = $q.defer();
      var source;
      if (type === 'camera') {
        source = navigator.camera.PictureSourceType.CAMERA;
      } else {
        source = navigator.camera.PictureSourceType.PHOTOLIBRARY;
      }
      var options = {
        quality: 75,
        // destinationType : Camera.DestinationType.FILE_URI,
        destinationType: navigator.camera.DestinationType.FILE_URI,
        sourceType: source,
        // allowEdit : true,
        encodingType: Camera.EncodingType.JPEG,
        correctOrientation: true,
        targetWidth: 300,
        targetHeight: 300,
        // popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };

      self.getPic(options).then(function (resp) {
        q.resolve(resp);
      }, function (err) {
        if (err === 'Camera cancelled') {
          q.reject('Camera cancelled');
        } else {
          q.reject(err);
        }
      }, function () {
      });
      return q.promise;
    };

    return self;
  }
})();
