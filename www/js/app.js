var app=angular.module('starter', [
    'ionic', 'ngCordova',
    'ngCordovaOauth',
    'ion-gallery',
    'ngSanitize',
    'com.2fdevs.videogular',
    'com.2fdevs.videogular.plugins.controls',
    'com.2fdevs.videogular.plugins.overlayplay',
    'com.2fdevs.videogular.plugins.poster',
    'ionic-circular-menu',
    'ionic.rating',
    'uiGmapgoogle-maps',
    'starter.controllers',
    'starter.directives',
    'starter.services',
    'ngMask',
    'base64',
    'pdf',
    'ngOpenFB',
    'wecheck.setting',
    'wecheck',
    'ionic.cloud',
    'ionic-ratings'
])
// "info.vietnamcode.nampnq.videogular.plugins.youtube",
.run(function($ionicPlatform,Setting, $state, $ionicSideMenuDelegate, $timeout, $ionicHistory, $cordovaPush, $rootScope, ngFB, $cordovaDialogs, $cordovaSQLite, errorPopup, wecheckStorage, wecheckApi) {

    $rootScope.newMailCount = 0;
    ngFB.init({appId: '541176595984451'});

    if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
    }

    Setting.init();

    if (window.StatusBar) {
        StatusBar.styleDefault();
    }
    if (Setting.getData('logged_in')) {
        $state.go('app.home');
    } else {
        $state.go('default.login');
    }

    var notificationOpenedCallback = function(jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    };

    if (window.plugins && window.plugins.OneSignal) {
        window.plugins.OneSignal
            .startInit("3028a203-ad71-4f5f-8655-0f4a0c96a987")
            .handleNotificationOpened(notificationOpenedCallback)
            .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
            .endInit();
           
    }
    
    // window.plugins.OneSignal.enableNotificationWhenActive(true);
    var androidConfig = {
        "senderID": "667320175870"
    };

    var iosConfig = {
        "badge": true,
        "sound": true,
        "alert": true,
    };

    try{
        wecheckStorage.openDB();
    } catch(e) {
        wecheckApi.logError(e.toString());
        // errorPopup.toast(e, 3000);
    }

    document.addEventListener("deviceready", function() {
        if ($ionicPlatform.is('android')) {
            $cordovaPush.register(androidConfig).then(function(result) {
                console.log("::cordovaPush register success:: " + result);
            }, function(err) {
                console.error("::cordovaPush register error::" + err);
            });
        } else {
            $cordovaPush.register(iosConfig).then(function(deviceToken) {
                console.log("::cordovaPush register success:: deviceToken: " + deviceToken)
                if (!window.localStorage['regid']) {
                    console.warn(deviceToken);
                    window.localStorage['regid'] = deviceToken;
                }
            }, function(err) {
                console.error(err);
            });
        }
        $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
            console.info('this is notificationReceived ' + notification);
            console.log(event);
            switch (notification.event) {
                case 'registered':
                    if (notification.regid.length > 0) {
                        if (!window.localStorage['regid']) {
                            console.warn(notification.regid);
                            window.localStorage['regid'] = notification.regid;
                        }
                    }
                    break;
                case 'message':
                   $cordovaDialogs.alert(notification.payload.alert, notification.payload.title, 'OK').then(function() {});
                    break;
                case 'error':
                    alert('GCM error = ' + notification.msg);
                    break;
                default:
                    alert('An unknown GCM event has occurred');
                    break;
            }
        });

    }, false);

    var backbutton = 0;
    $ionicPlatform.registerBackButtonAction(function(event) {
        if ($rootScope.searchModal!=null) {
            $rootScope.searchModal.hide();
            $rootScope.searchModal=null;
        }
        else if($ionicSideMenuDelegate.isOpenLeft()) { 
            $ionicSideMenuDelegate.toggleLeft();
        }
        else {
            var alert = document.querySelector(".sweet-alert");
            if (alert!=null) {
                okButton = alert.getElementsByTagName("button")[0];
                $(okButton).trigger("click");
            }
            switch ($state.current.name) {
                  case 'app.myRewards':
                    $state.go('app.home');
                    break;
                 case 'app.profile':
                    $state.go('app.home');
                    break;
                case 'default.login':
                    navigator.app.exitApp();
                    break;
                case 'default.signup':
                    $ionicHistory.goBack();
                    break;
                case 'app.home':
                        if (backbutton === 0) {
                            backbutton++;
                            window.plugins.toast.showShortBottom('Press again to exit');
                            $timeout(function() {
                                backbutton = 0;
                            }, 5000);
                        } else {
                            navigator.app.exitApp();
                        }
                    break;
                default:
                    $ionicHistory.goBack();
                    break;
            }
        }
    }, 401);
})

.config(function($ionicCloudProvider) {
    $ionicCloudProvider.init({
        "core": {
            "app_id": "aedefbb0"
        },
        "auth": {
            "facebook": {
            "scope": []
        },
        "google": {
          "webClientId": "981691518423-2sp3coengfcoqjd5kj0jtdhero75rcln.apps.googleusercontent.com",
          "scope": ["permission1", "permission2"]
        }
        }
    });
})

.service('httpInterceptor', ['$q','$injector', 'Setting', function ($q, $injector, Setting) {
    this.request = function (config) {
        if (config.url.indexOf('wecheckapi') > 0) {
            if(config.url.indexOf('venues') <0 && config.url.indexOf('instagram') <0 && config.url.indexOf('messages') <0) {
                $injector.get('$ionicLoading').show({templateUrl: 'templates/spinner.html'});
            }  
        }
        return config || $q.when(config);
    };

    this.response = function (response) {
        if (response.config.url.indexOf('wecheckapi') > 0) {
            $injector.get('$ionicLoading').hide();
        }
        return response || $q.when(response);
    };

    this.responseError = function (response) {
        if (response.config.url.indexOf('wecheckapi') > 0) {
            $injector.get('$ionicLoading').hide();
            $injector.get('errorPopup').toast(response.data);
        }
        if (response.status === 401) {
            Setting.clear();
            $injector.get('$location').path('/default/login');
        }
        return $q.reject(response);
    };
}])

.config(['$compileProvider', '$httpProvider', function ($compileProvider, $httpProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|sms|mailto|file|tel):/);
    $httpProvider.interceptors.push('httpInterceptor');
}]);

var callback = function () {
    angular.bootstrap(document, ['starter']);
};

if (window.cordova) {
    window.ionic.Platform.ready(callback);
}
else {
    window.addEventListener('load', callback, false);
}
