app.controller("MainCtrl",function($scope,$cordovaBarcodeScanner,$rootScope,$state){

    $scope.check_gps=function() {
      if (window.cordova) {
        cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
          if (enabled == false) {
            swal({
              title: "GPS Enable",
              text: "Wecheck wants to access your location while you use the app?",
              type: "warning",
              confirmButtonColor: "#800000",
              confirmButtonText: "Enable",
              closeOnConfirm: true   
            },
            function(){
                cordova.plugins.diagnostic.switchToLocationSettings();
            });
          }
        }, function(error) {
            console.error(error);
            // alert("The following error occurred: " + error);
        });
     }
    }

    $scope.check_wifi=function() {
      if (window.cordova) {
        cordova.plugins.diagnostic.isWifiEnabled(function(enabled) {
          if (enabled == false) {
            swal({
              title: "Wifi Connectivity",
              text: "Wecheck need the wifi connectivity to work faster the app?",
              type: "warning",
              confirmButtonColor: "#800000",
              confirmButtonText: "Enable",
              closeOnConfirm: true
            },
            function(){
                cordova.plugins.diagnostic.switchToWifiSettings();
            });
          }
          else {
               $scope.check_gps(); 
          }
        }, function(error) {
            // console.error(error);
            // alert("The following error occurred: " + error);
        });
      }
    }

    // $scope.footer="<div class='tabs tabs-icon-only footer-tab'><a class='tab-item' href='#/app/home'><i class='icon-tab-home'></i></a><a class='tab-item' ui-sref='app.scanQR'><i class='icon-tab-qrcode'></i></a><a class='tab-item' ng-click='openSearchModal()'><i class='icon-tab-search'></i></a><a class='tab-item tab-item-inbox' href='#/app/inbox'><i class='icon-tab-mail'></i><span class='badge-mail-count badge badge-assertive' ng-if='newMailCount>0' ng-bind='newMailCount'></span></a><a class='tab-item' href='#/app/map'><i class='icon-tab-places'></i></a></div>";


     $scope.scan_barcode = function(){
   
    // document.addEventListener("deviceready", function () {

    $cordovaBarcodeScanner
      .scan()
      .then(function(barcodeData) {
        console.log(barcodeData)
        $rootScope.mybarcode_data = barcodeData;
       $state.go("app.scanQR");
      }, function(error) {
        console.log(error)
      });


   
    $cordovaBarcodeScanner
      .encode(BarcodeScanner.Encode.TEXT_TYPE, "http://www.nytimes.com")
      .then(function(success) {
        console.log(success)
      
      }, function(error) {
        console.log(error)
       
      });

  // }, false);
  };





});