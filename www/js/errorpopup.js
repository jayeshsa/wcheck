angular.module('starter.services')
.factory('errorPopup', function($ionicPopup, $cordovaToast){
  var self = this;

  self.showPopup = function(dtitle,message,type){
    if(type==null || type=="info")
    {
      swal({   title: dtitle,   text:message,    confirmButtonText: "OK",confirmButtonColor: "#800000", })
    }
    else
    {
      swal({   title: dtitle,   text:message,   type: "error",   confirmButtonText: "OK",confirmButtonColor: "#800000", });
    }
  };

  self.noInternet = function(){swal({   title: "Error!",   text:"Sorry, no Internet connectivity detected. Please reconnect and try agian",   type: "error",   confirmButtonText: "OK" });};

  self.toast = function (message) {
    $cordovaToast.showLongBottom(message).then(function () {
    }, function () {
      // error
    });
  };

  self.toastmiddle = function (message) {
    $cordovaToast.showLongCenter(message).then(function () {
    }, function () {
      // error
    });
  };
  self.showShortTop = function (message) {
    $cordovaToast.showShortTop(message).then(function () {
    }, function () {
      // error
    });
  };

  return self;
})
