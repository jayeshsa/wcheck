app.service('Alert', function($mdDialog,$ionicPopup,$timeout,ClosePopupService,$mdToast) {
 
        this.show=function(dialog_title,dialog_message)
        {
           var popup= $ionicPopup.alert({
                title: dialog_title,
                cssClass: 'alert_dialog',
                template: dialog_message
            });
            
             ClosePopupService.register(popup);
        }
    
})

app.service('Loading', function($ionicPopup,$timeout,ClosePopupService,$ionicLoading) {
//  <div class="loader loading_dialog"><ion-spinner icon="android"></ion-spinner></div>
        this.show=function()
        {
             $ionicLoading.show({
                 template: '<div class="spinner"><div class="ball"></div><p>Loading</p></div>'
             });
        }
     
        this.hide=function()
        {
           $ionicLoading.hide();
        }
    
})